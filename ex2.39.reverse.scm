(load "ex2.38.fold.scm")

(define nil '())

(define (r1 seq)
  (fold-right (lambda (x y)
                      (cond ((null? y) x)
                            ((not (pair? y)) (list y x))
                            (else (append y (list x)))))
              nil seq))

(define (r2 seq)
  (fold-left (lambda (x y) (cons y x)) nil seq))
