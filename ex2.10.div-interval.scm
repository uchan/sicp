(load "ex2.8.sub-interval.scm")

(define (div-interval x y)
  (if (> 0 (* (upper-bound y) (lower-bound y)))
      (display "error")
      (mul-interval x
                    (make-interval (/ 1.0 (upper-bound y)
                                   (/ 1.0 (lower-bound y)))))))
