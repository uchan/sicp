(load "5.2.1.machine-model.scm")

;; b)
(define ct-machine
  (make-machine
    '(tree continue n)
    (list (list 'null? null?) (list 'pair? pair?) (list 'car car) (list 'cdr cdr) (list '+ +))
    '(controller
        (assign n (const 0))
        (assign continue (label ct-done))
      ct-loop
        (test (op null?) (reg tree))
        (branch (label null-case))
        (test (op pair?) (reg tree))
        (branch (label else-case))
        ;; base case 2: atom
        (assign n (op +) (reg n) (const 1))
        (goto (reg continue))
      null-case
        ;; base case 1: null
        (goto (reg continue))
      else-case
        (save continue)
        (save tree)
        ;; set for loop: car
        (assign tree (op car) (reg tree))
        (assign continue (label after-car))
        (goto (label ct-loop))
      after-car
        ;; set for loop: cdr
        (restore tree)
        ;; current n = (count-iter (car tree) old-n)
        (assign tree (op cdr) (reg tree))
        (restore continue)
        (goto (label ct-loop))
      ct-done)))
;  setting input
(newline)
(set-register-contents! ct-machine 'tree '(1 (2 3 (4))))
(start ct-machine)
(display "ct-machine result:")
(display (get-register-contents ct-machine 'n))
(newline)
