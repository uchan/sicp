(load "4.3.3.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;; pre-defined
(define (require p) (if (not p) (amb)))
(define (an-integer-staring-from n)
  (amb n (an-integer-staring-from (+ n 1))))
(define (a-pythagorean-triple-between low high)
  (let ((i (an-integer-between low high)))
       (let ((j (an-integer-between i high)))
            (let ((k (an-integer-between j high)))
                 (require (= (+ (* i i) (* j j)) (* k k)))
                 (list i j k)))))
;; begin test
(define (an-integer-between low high)
  (require (or (= low high) (< low high)))
  (amb low (an-integer-between (+ low 1) high)))
(a-pythagorean-triple-between 3 20)
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (begin (let ((input (car test-code)))
                  (set! test-code (cdr test-code))
                  (display input)(newline)
                  input))
      (sys-read)))

(driver-loop)
