(load "ex4.6.let.scm")

;; (let var ((v1 e1) (v2 e2)) <body>)
;; using define, becaus <body> may contain 'var
;; ==>(begin (define (var v1 v2) <body>) (var e1 e2))
;; but 'define may have nameclash issue, so, put it in 'lambda
;; ==>((lambda () (define (var v1 v2) <body>) (var e1 e2)))

;; helper
(define (lete-var exp) (cadr exp))
(define (lete-vars exp) (map car (caddr exp)))
(define (lete-vals exp) (map cadr (caddr exp)))
(define (lete-body exp) (cdddr exp))

(define (make-define var params body)
  (cons 'define (cons (cons var params) body)))
;; answer
(define (let-var-ext exp)
  (list (make-lambda '() (list (make-define (lete-var exp) (lete-vars exp) (lete-body exp))
                               (cons (lete-var exp) (lete-vals exp))))))

(define (let->combination exp)
  (if (symbol? (lete-var exp))
      (let-var-ext exp)
      (cons (make-lambda (let-vars exp) (let-body exp))
            (let-vals exp))))

;; tester
(driver-loop)
;; test code
;; (let func ((x (quote 1 2))) (if (null? (cdr x)) x (func (cdr x))))
