;; text book
(define (enumerate-interval low high)
  (if (> low high) '()
      (cons low (enumerate-interval (+ low 1) high))))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))

;; question
(define (queens board-size)
;;(show "queens" board-size)
  (define (queen-cols k)
  ;;(show "queen-cols" k)
    (if (= k 0)
        (list empty-board)
        (filter (lambda (positions) (safe? k positions))
                (flatmap (lambda (rest-of-queens)
                                 (map (lambda (new-row) (adjoin-position new-row k rest-of-queens))
                                      (enumerate-interval 1 board-size)))
                         (queen-cols (- k 1))))))
  (queen-cols board-size))

;; answer
(define empty-board '())

(define (safe? k positions)
;;(show "safe?" k positions)
  (define (check i left)
    (cond ((null? left) #t)
          ((= (car positions) (car left)) #f)
          ((= (abs (- (car positions) (car left)))
              (- k i))
           #f)
          (else (check (- i 1) (cdr left)))))
  (cond ((null? positions) #t)
        ((null? (cdr positions)) #t)
        (else (check (- k 1) (cdr positions)))))

(define (adjoin-position new-row k rest-of-queens)
;;(show "adjoin-position" new-row k rest-of-queens)
  (cons new-row rest-of-queens))

;; helper
(define (show func . datas)
  (display func)(display ", data:")
  (map (lambda (d) (display d)(display ",")) datas)
  (newline))
