(load "5.2.1.machine-model.scm")

;; answer
(define (make-execution-procedure inst labels machine pc flag stack ops)
  (cond ((eq? (car inst) 'assign) (make-assign inst machine labels ops pc))
        ((eq? (car inst) 'test) (make-test inst machine labels ops flag pc))
        ((eq? (car inst) 'branch) (make-branch inst machine labels flag pc))
        ((eq? (car inst) 'goto) (make-goto inst machine labels pc))
        ((eq? (car inst) 'save) (make-save inst machine stack pc))
        ((eq? (car inst) 'restore) (make-restore inst machine stack pc))
        ((eq? (car inst) 'clear) (make-clear machine stack pc))
        ((eq? (car inst) 'perform) (make-perform inst machine labels ops pc))
        (else (error "[ex]Unknown instruction type -- ASSEMBLE" inst))))

(define (make-clear machine stack pc)
  (lambda ()
    (stack 'initialize)
    (advance-pc pc)))

;; test
(define my-machine
  (make-machine
    '(a)
    (list (list 'print display))
    '(start
       (assign a (const 1))
       (perform (op print) (reg a))
       (save a)
       (clear)
       (restore a)
       (perform (op print) (reg a))
     end)
  )
)
;  setting input
(start my-machine)
(display "machine result:")
(display (get-register-contents my-machine 'a))
(newline)
