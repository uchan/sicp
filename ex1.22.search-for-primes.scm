;; functions
(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (real-time-clock)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (real-time-clock) start-time))
      #f)) ;; fix return false

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

;; my work
(define (search-one-prime minn)
  (if (even? minn)
      (search-one-prime (+ 1 minn))
      (if (timed-prime-test minn) minn (search-one-prime (+ minn 2)))))

(define (search-num-primes minn num)
  (cond ((= 1 num) (search-one-prime minn))
        (else (search-num-primes (+ 1 (search-one-prime minn))
                                 (- num 1)))))

(define (search-for-primes minn)
  (search-num-primes minn 3))
