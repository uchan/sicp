;;text book
(define (make-mobile left right)
  (list left right))

(define (make-branch len structure)
  (list len structure))


;; a)
(define (left-branch mobi) (car mobi))
(define (right-branch mobi) (car (cdr mobi)))
(define (branch-length branch) (car branch))
(define (branch-structure branch) (car (cdr branch)))


;; b)
(define (total-weight stru)
  (if (not (pair? stru)) stru
      (+ (total-weight (branch-structure (left-branch stru)))
         (total-weight (branch-structure (right-branch stru))))))

;; c)
;; helper function
(define (left-stru mobi) (branch-structure (left-branch mobi)))
(define (right-stru mobi) (branch-structure (right-branch mobi)))
(define (left-len mobi) (branch-length (left-branch mobi)))
(define (right-len mobi) (branch-length (right-branch mobi)))

(define (balance? stru)
  (if (not (pair? stru)) #t
      (and (= (* (total-weight (left-stru stru)) (left-len stru))
              (* (total-weight (right-stru stru)) (right-len stru)))
           (balance? (left-stru stru))
           (balance? (right-stru stru)))))


;; d)
;; text book
(define (make-mobile left right) (cons left right))
(define (make-branch len structure) (cons len structure))
;; answer
(define (left-branch mobi) (car mobi))
(define (right-branch mobi) (cdr mobi))
(define (branch-length branch) (car branch))
(define (branch-structure branch) (cdr branch))
