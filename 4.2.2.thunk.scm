(load "4.1.1.scm")

;; new tools we need
(define (delay-it exp env)
  (list 'thunk exp env))

(define (thunk? obj) (tagged-list? obj 'thunk))
(define (thunk-exp thunk) (cadr thunk))
(define (thunk-env thunk) (caddr thunk))

(define (evaluated-thunk? obj) (tagged-list? obj 'evaluated-thunk))
(define (thunk-value evaluated-thunk) (cadr evaluated-thunk))

(define (actual-value exp env)
  (force-it (g-eval exp env)))

(define (force-it obj) (force-it-simple obj)) ;; [g]switch Implementation here

(define (force-it-simple obj)
  (if (thunk? obj)
      (actual-value (thunk-exp obj) (thunk-env obj))
      obj))

(define (force-it-mem obj)
  (cond ((thunk? obj)
         (let ((result (actual-value (thunk-exp obj) (thunk-env obj))))
              (set-car! obj 'evaluated-thunk)
              (set-car! (cdr obj) result) ;; replace <exp> with its <value>
              (set-cdr! (cdr obj) '())    ;; forget unneeded <env>
              result))
        ((evaluated-thunk? obj) (thunk-value obj))
        (else obj)))

;; new eval
(define (g-eval exp env)
  (cond ((self-evaluating? exp) exp)
        ((variable? exp) (lookup-variable-value exp env))
        ((quoted? exp) (text-of-quotation exp))
        ((assignment? exp) (eval-assignment exp env))
        ((definition? exp) (eval-definition exp env))
        ((if? exp) (eval-if exp env))
        ((lambda? exp) (make-procedure (lambda-parameters exp)
                                       (lambda-body exp)
                                       env))
        ((begin? exp) (eval-sequence (begin-actions exp) env))
        ((cond? exp) (g-eval (cond->if exp) env))
        ((application? exp) (g-apply (actual-value (operator exp) env)
                                     (operands exp)
                                     env))
        (else (error "[L-env]Unknown expression type -- EVAL" exp))))

;; new apply inners
(define (list-of-arg-values exps env)
  (if (no-operands? exps)
      '()
      (cons (actual-value (first-operand exps) env)
            (list-of-arg-values (rest-operands exps) env))))

(define (list-of-delayed-args exps env)
  (if (no-operands? exps)
      '()
      (cons (delay-it (first-operand exps) env)
            (list-of-delayed-args (rest-operands exps) env))))
;; new apply
(define (g-apply procedure arguments env)
  (cond ((primitive-procedure? procedure) (apply-primitive-procedure procedure (list-of-arg-values arguments env)))
        ((compound-procedure? procedure) (eval-sequence (procedure-body procedure)
                                                        (extend-environment
                                                          (procedure-parameters procedure)
                                                          (list-of-delayed-args arguments env)
                                                          (procedure-environment procedure))))
        (else (error "[L-env] Unknown procedure type --- APPLY" procedure))))

;; new eval-if
(define (eval-if exp env)
  (if (true? (actual-value (if-predicate exp) env))
      (g-eval (if-consequent exp) env)
      (g-eval (if-alternative exp) env)))

;; new driver-loop
(define input-prompt ";;; L-Eval input:")
(define output-prompt ";;; L-Eval value:")
(define (driver-loop)
  (prompt-for-input input-prompt)
  (let ((input (read)))
       (let ((output (actual-value input the-global-environment)))
            (announce-output output-prompt)
            (user-print output)))
  (driver-loop))
