(load "ex1.37.cont-frac.scm")

(define (square n) (* n n))

(define (tan-cf x k)
  (define (n i)
    (if (= 1 i) x
        (- 0 (square x))))
  (define (d i) (- (* 2 i) 1))
  (cont-frac n d k))
