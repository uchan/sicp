(define (plus2 a b)
	(if (= a 0)
	    b
	    (plus2 (dec a) (inc b))))
