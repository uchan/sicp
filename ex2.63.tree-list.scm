(define x '(7 (3 (1 () ()) (5 () ())) (9 () (11 () ()))))

(define (tree->list-1 tree)
  (if (null? tree) '()
      (append (tree->list-1 (cadr tree))
              (cons (car tree) (tree->list-1 (caddr tree))))))


(define (tree->list-2 tree)
  (define (copy-to-list tree result)
    (if (null? tree) result
        (copy-to-list (cadr tree)
                      (cons (car tree) (copy-to-list (caddr tree) result)))))
  (copy-to-list tree '()))
