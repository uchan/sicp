(load "3.5.1.stream.scm")

(define (show x) (display-line x) x)

(display-line 'begin-of-test)
(define x (stream-map show (stream-enumerate-interval 0 10)))
(display-line 'after-define-x)
(display-line (stream-ref x 5))
(display-line 'after-ref-5)
(display-line (stream-ref x 7))
(display-line 'after-ref-7)
