(load "ex3.70.merge.scm")

(define (weight-r pair)
  (define (tri x) (* x x))
  (+ (tri (car pair)) (tri (cadr pair))))

(define r-num-pairs (weighted-pairs integers integers weight-r))

(define (r-num)
  (define (iter s)
    (let ((s0 (stream-ref s 0)) (s1 (stream-ref s 1)) (s2 (stream-ref s 2)))
         (if (and (= (weight-r s0) (weight-r s1)) (= (weight-r s1) (weight-r s2)))
             (cons-stream (list (weight-r s0) s0 s1 s2)
                          (iter (stream-cdr s)))
             (iter (stream-cdr s)))))
  (iter r-num-pairs))


;; test code
(display-line 'ex3.72)
(display-line (stream-head r-num-pairs 20))
(display-line (stream-head (r-num) 10))
