(load "5.2.1.machine-model.scm")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; exp func
; have label : (('info text label). proc)
;   no label : (('info text) . proc)
(define (make-instruction text)
  (cons (list 'info text) '()))
(define (instruction-text inst)
  (let ((info (car inst)))
    (cadr info)))

;; helper
(define (insert-label inst label)
  (let ((info (car inst)))
       (cons (append info (list label)) (cdr inst)))) ;; return single instruction
(define (make-new-insts insts label)
  (if (null? insts)
      insts
      (let ((first-inst (car insts)))
           (cons (insert-label first-inst label) (cdr insts))))) ;; return fixed instruction sequence

;; save label info
(define (extract-labels text receive)
  (if (null? text)
      (receive '() '())
      (extract-labels (cdr text)
        (lambda (insts labels)
          (let ((next-inst (car text)))
               (if (symbol? next-inst)
                   (let ((new-insts (make-new-insts insts next-inst)))
                        (receive new-insts (cons (make-label-entry next-inst new-insts) labels)))
                   (receive (cons (make-instruction next-inst) insts) labels)))))))

;; show inst
(define (show-inst inst)
  (define (show-inst-tag) (display "\n[trace]"))
  (let ((info (car inst)))
       (if (null? (cddr info))
           (begin (show-inst-tag) (display (cadr info))) ;; show instruction only
           (begin (show-inst-tag) (display (caddr info)) ;; show label and instruction
                  (show-inst-tag) (display (cadr info))))))

;; machine with counter
(define (make-new-machine)
  (let ((pc (make-register 'pc))
        (flag (make-register 'flag))
        (stack (make-stack))
        (the-instruction-sequence '())
        (counter 0))
       (let ((the-ops (list (list 'initialize-stack (lambda () (stack 'initialize)))))
             (register-table (list (list 'pc pc) (list 'flag flag))))
            (define (allocate-register name)
              (if (assoc name register-table)
                  (error "Multiply definede register:" name)
                  (set! register-table (cons (list name (make-register name))
                                             register-table)))
              'register-allocated)
            (define (lookup-register name)
              (let ((val (assoc name register-table)))
                   (if val (cadr val) (error "Unknown register: " name))))
            (define (execute)
              (let ((insts (get-contents pc)))
                   (if (null? insts)
                       'done
                       (begin (show-inst (car insts))
                              ((instruction-execution-proc (car insts)))
                              (set! counter (+ counter 1))
                              (execute)))))
            (define (show-counter) (display (list 'counter: counter))(newline))
            (define (reset-counter) (set! counter 0))
            (define (dispatch message)
              (cond ((eq? message 'start)
                     (set-contents! pc the-instruction-sequence)
                     (execute))
                    ((eq? message 'install-instruction-sequence)
                     (lambda (seq) (set! the-instruction-sequence seq)))
                    ((eq? message 'allocate-register) allocate-register)
                    ((eq? message 'get-register) lookup-register)
                    ((eq? message 'install-operations)
                     (lambda (ops) (set! the-ops (append the-ops ops))))
                    ((eq? message 'stack) stack)
                    ((eq? message 'operations) the-ops)
                    ((eq? message 'show-counter) (show-counter))
                    ((eq? message 'reset-counter) (reset-counter))
                    (else (error "Unknown request -- MACHINE" message))))
            dispatch)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; test
(load "5.1.4.fibonacci.scm")
;  setting input
(define (run n)
  (newline)
  (display (list 'input: n))
  (set-register-contents! fib-machine 'n n)
  (start fib-machine)
  (display "\nmachine result:")
  (display (get-register-contents fib-machine 'val))
  (newline)
  (fib-machine 'show-counter)
  (fib-machine 'reset-counter))
; run
(run 2)
