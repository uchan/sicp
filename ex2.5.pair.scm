(define (iter n z a)
  (if (not (= 0 (remainder z n)))
      a
      (iter n (/ z n) (+ a 1))))

(define (pcons a b) (* (expt 2 a) (expt 3 b)))
(define (pcar z) (iter 2 z 0))
(define (pcdr z) (iter 3 z 0))
