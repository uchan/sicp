(load "5.2.1.machine-model.scm")

;; test before
(define my-machine
  (make-machine
    '(a)
    (list (list '- -) (list '+ +))
    '(start
        (assign a (op -) (const 1) (label end))
     end)
  )
)
;;(display "test before:\n")
;;(start my-machine) <-- error

;; answer
(define (make-operation-exp exp machine labels operations)
  (let ((op (lookup-prim (operation-exp-op exp) operations))
        (aprocs (map (lambda (e)
                        (if (label-exp? e) (error "[ex] can not operate 'label':" e))
                        (make-primitive-exp e machine labels))
                     (operation-exp-operands exp))))
       (lambda () (apply op (map (lambda (p) (p)) aprocs)))))

;; test after
(define my-machine
  (make-machine
    '(a)
    (list (list '- -) (list '+ +))
    '(start
        (assign a (op -) (const 1) (label end))
     end)
  )
)
(display "test after:\n")
(start my-machine)
