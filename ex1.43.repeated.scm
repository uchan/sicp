(define (repeated f n)
  (define (iter i out)
    (if (= i 1) out
        (iter (- i 1) (lambda (x) (f (out x))))))
  (iter n f))
