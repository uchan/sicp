(load "4.2.2.thunk.scm")

;;a
;(define (for-each proc items)
;  (if (null? items)
;      'done
;      (begin (proc (car items))
;             (for-each proc (cdr items)))))
;
;(for-each (lambda (x) (newline) (display x)) (list 57 321 88))

(define (eval-sequence exps env)
  (cond ((last-exp? exps) (g-eval (first-exp exps) env))
        (else (actual-value (first-exp exps) env)
              (eval-sequence (rest-exps exps) env))))
