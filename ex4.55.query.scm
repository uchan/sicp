(load "4.4.4.query-system.scm")
(load "4.4.1.database.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;a)
(supervisor ?name (Bitdiddle Ben))
;b)
(job ?name (accounting . ?position))
;c)
(address ?name (Slumerville . ?detail))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
