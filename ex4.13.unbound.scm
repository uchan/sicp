(load "4.1.1.scm")

(define (appe lst elm)
  (append lst (list elm)))

;; expression
(define (unbound? exp) (tagged-list? exp 'unbound!))
(define (unbound-var exp) (cadr exp))

;; process
(define (eval-unbound exp env)
  (let ((frame (first-frame env))
        (target-var (unbound-var exp)))
       (define (loop vars vals out-vars out-vals)
         (cond ((null? vars) (make-frame out-vars out-vals))
               ((eq? target-var (car vars)) (loop (cdr vars) (cdr vals) out-vars out-vals))
               (else (loop (cdr vars) (cdr vals)
                           (appe out-vars (car vars)) (appe out-vals (car vals))))))
       (let ((result (loop (frame-variables frame) (frame-values frame) '() '())))
            (set-car! frame (frame-variables result))
            (set-cdr! frame (frame-values result))))
  'ok)

;; eval
(define (g-eval exp env)
  (cond ((self-evaluating? exp) exp)
        ((variable? exp) (lookup-variable-value exp env))
        ((quoted? exp) (text-of-quotation exp))
        ((assignment? exp) (eval-assignment exp env))
        ((definition? exp) (eval-definition exp env))
        ((if? exp) (eval-if exp env))
        ((lambda? exp) (make-procedure (lambda-parameters exp)
                                       (lambda-body exp)
                                       env))
        ((begin? exp) (eval-sequence (begin-actions exp) env))
        ((cond? exp) (g-eval (cond->if exp) env))
        ((unbound? exp) (eval-unbound exp env))
        ((application? exp) (g-apply (g-eval (operator exp) env)
                                     (list-of-values (operands exp) env)))
        (else (error "[M-env]Unknown expression type -- EVAL" exp))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; tester
(driver-loop)
;; test code
(define (make-mem test-val)
  (lambda (func)
    (cond ((equal? func "test") (lambda (x) (set! test-val x)))
          (else test-val))))

(define t (make-mem 100))
(define x (quote 1 2 3))
((t "test") x)
(unbound! x)
(t "")
