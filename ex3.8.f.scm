(define (makef)
  (define value 0)
  (lambda (new)
    (let ((old value))
         (begin (set! value new) old))))

(define f (makef))
