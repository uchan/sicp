;; code
(define (appe x y)
  (if (null? x)
      y
      (cons (car x) (appe (cdr x) y))))
(define (appe! x y)
  (set-cdr! (last-pair x) y) x)
(define (last-pair x)
  (if (null? (cdr x))
      x
      (last-pair (cdr x))))

;; test
(define x (list 'a 'b))
(define y (list 'c 'd))
(define z (appe x y))
