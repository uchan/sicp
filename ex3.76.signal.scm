(load "ex3.50.stream-map.scm")
(load "3.5.3.paradigm.scm")

;; code not in book
(define (make-stream-from-list lst)
  (if (null? lst)
      the-empty-stream
      (cons-stream (car lst) (make-stream-from-list (cdr lst)))))
(define sense-data (make-stream-from-list '(1 2 1.5 1 0.5 -0.1 -2 -3 -2 -0.5 0.2 3 4)))

(define (sign-change-detector second first)
  (cond ((> (* second first) 0) 0)
        ((and (= first 0) (= second 0)) 0)
        ((or (and (= first 0) (> second 0))
             (and (> first 0) (= second 0))) 0)
        ((and (< first 0) (= second 0)) 1)
        ((and (= first 0) (< second 0)) -1)
        ((and (> first 0) (< second 0)) -1)
        ((and (< first 0) (> second 0)) 1)
        (else (display-line 'errer-in-sign-change-detector))))

;; answers
(define (smooth s)
  (if (null? (stream-car (stream-cdr s)))
      the-empty-stream
      (let ((s0 (stream-ref s 0)) (s1 (stream-ref s 1)))
           (cons-stream (/ (+ s0 s1) 2)
                        (smooth (stream-cdr s))))))

(define (make-zero-crossings input transform)
  (define real-input (transform input))
  (stream-map-ext sign-change-detector real-input (cons-stream 0 real-input)))

(define zero-crossings (make-zero-crossings sense-data smooth))

;; test code
(display-line "ex3.76")
(display-line (stream-head sense-data 13))
(display-line (stream-head zero-crossings 12))
