(load "3.5.2.infinite.scm")

;; (a)
(define (integrate-series s)
     (stream-map-ext (lambda (x y) (* x (/ 1 y))) s integers))

;; tester
(display 'integrate-series-ones)(newline)
(display (stream-head (integrate-series ones) 20))(newline)

;; (b)
(define exp-series (cons-stream 1 (integrate-series exp-series)))
;; answer
(define cosine-series (cons-stream 1 (integrate-series (scale-stream sine-series -1))))
(define sine-series (cons-stream 0 (integrate-series cosine-series)))
;;tester
(display 'cosine)(newline)
(display (stream-head cosine-series 20))(newline)
(display 'sine)(newline)
(display (stream-head sine-series 20))(newline)
