(load "4.3.3.scm")

;;===============================================
;; answer code
;;===============================================
;; for 'if-fail' symbol
(define (if-fail? exp) (tagged-list? exp 'if-fail))
(define (if-fail-consequent exp) (cadr exp))
(define (if-fail-alternative exp) (caddr exp))

;-------------------------------------
; main answer
(define (analyze-if-fail exp)
  (let ((cproc (analyze (if-fail-consequent exp)))
        (aproc (analyze (if-fail-alternative exp))))
       (lambda (env succeed fail)
         (cproc env
                (lambda (val fail2) ; success for <if-fail>: return value
                  (succeed val fail2))
                (lambda ()          ; failure for <if-fail>: do alternative
                  (aproc env succeed fail))))))
;-------------------------------------

;; main function
(define (analyze exp)
  (cond ((self-evaluating? exp) (analyze-self-evaluating exp))
        ((quoted? exp) (analyze-quoted exp))
        ((variable? exp) (analyze-variable exp))
        ((assignment? exp) (analyze-assignment exp))
        ((definition? exp) (analyze-definition exp))
        ((if? exp) (analyze-if exp))
        ((if-fail? exp) (analyze-if-fail exp))
        ((not? exp) (analyze (not->if (get-aon-action exp))))
        ((or? exp) (analyze (or->if (get-aon-action exp))))
        ((and? exp) (analyze (and->if (get-aon-action exp))))
        ((lambda? exp) (analyze-lambda exp))
        ((begin? exp) (analyze-sequence (begin-actions exp)))
        ((cond? exp) (analyze (cond->if exp)))
        ((let? exp) (analyze-let exp))
        ((amb? exp) (analyze-amb exp))
        ((application? exp) (analyze-application exp))
        (else (error "[M-amb] Unknow expression type -- ANALYZE" exp))))

;;===============================================
;; test code
;;===============================================
(define test-flag true)
(define sys-read read)
(define test-code '(
;; pre-defined
(define (require p) (if (not p) (amb)))
(define (an-element-of items)
  (require (not (null? items)))
  (amb (car items) (an-element-of (cdr items))))
(define (even? n) (= 0 (remainder n 2)))
;; begin test
(if-fail (let ((x (an-element-of (quote 1 3 5 8))))
              (require (even? x))
              x)
         "all-odd")
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (begin (let ((input (car test-code)))
                  (set! test-code (cdr test-code))
                  (display input)(newline)
                  input))
      (sys-read)))

(driver-loop)
