(load "1.3.3.fixed-point.scm")
(define (gold) (fixed-point (lambda (x) (+ 1 (/ 1 x))) 1.0))

(define (cont-frac n d k)
  (define (f i)
    (if (= i k) (/ (n k) (d k))
        (/ (n i) (+ (d i) (f (+ i 1))))))
  (f 1))

(define (cont-frac-i n d k)
  (define (iter i result)
    (if (= i 0) result
        (iter (- i 1) (/ (n i) (+ (d i) result)))))
  (iter (- k 1) (/ (n k) (d k))))


;; tester
(define (t k) (cont-frac (lambda (i) 1.0) (lambda (i) 1.0) k))
(define (ti k) (cont-frac-i (lambda (i) 1.0) (lambda (i) 1.0) k))
