(load "4.3.3.scm")

;;========================================================
;; answer code
;;========================================================
;; for 'permanent-set!' symbol
(define (permanent-set? exp) (tagged-list? exp 'permanent-set!))
(define (permanent-set-variable exp) (cadr exp))
(define (permanent-set-value exp) (caddr exp))

;-------------------------------------
; main answer
(define (analyze-permanent-set exp)
  (let ((var (permanent-set-variable exp))
        (vproc (analyze (permanent-set-value exp))))
       (lambda (env succeed fail)
         (vproc env
                (lambda (val fail2) ; success for <permanent-set>: set value
                  (set-variable-value! var val env)
                  (succeed 'ok fail2))
                fail))))
;-------------------------------------

;; main function
(define (analyze exp)
  (cond ((self-evaluating? exp) (analyze-self-evaluating exp))
        ((quoted? exp) (analyze-quoted exp))
        ((variable? exp) (analyze-variable exp))
        ((assignment? exp) (analyze-assignment exp))
        ((permanent-set? exp) (analyze-permanent-set exp))
        ((definition? exp) (analyze-definition exp))
        ((if? exp) (analyze-if exp))
        ((not? exp) (analyze (not->if (get-aon-action exp))))
        ((or? exp) (analyze (or->if (get-aon-action exp))))
        ((and? exp) (analyze (and->if (get-aon-action exp))))
        ((lambda? exp) (analyze-lambda exp))
        ((begin? exp) (analyze-sequence (begin-actions exp)))
        ((cond? exp) (analyze (cond->if exp)))
        ((let? exp) (analyze-let exp))
        ((amb? exp) (analyze-amb exp))
        ((application? exp) (analyze-application exp))
        (else (error "[M-amb] Unknow expression type -- ANALYZE" exp))))
;;========================================================
;; test code
;;========================================================
(define test-flag true)
(define sys-read read)
(define test-code '(
;; pre-defined
(define (require p) (if (not p) (amb)))
(define (an-element-of items)
  (require (not (null? items)))
  (amb (car items) (an-element-of (cdr items))))
;; begin test
(define count 0)
(let ((x (an-element-of (quote a b c)))
      (y (an-element-of (quote a b c))))
     (permanent-set! count (+ count 1))
     (require (not (equal? x y)))
     (list x y count))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (begin (let ((input (car test-code)))
                  (set! test-code (cdr test-code))
                  (display input)(newline)
                  input))
      (sys-read)))

(driver-loop)
