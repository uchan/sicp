(load "4.1.1.scm")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; thunk
(define (thunk? obj) (tagged-list? obj 'thunk))
;; type: <lazy> or <lazy-memo>
(define t-type-l 'lazy)
(define t-type-m 'lazy-memo)
(define (thunk-type thunk) (cadr thunk))
(define (thunk-exp thunk) (caddr thunk))
(define (thunk-env thunk) (cadddr thunk))
;; helpers
(define (make-thunk type exp env) (list 'thunk type exp env))
(define (replace-exp obj val) (set-car! (cddr obj) val))
(define (replace-env obj val) (set-car! (cdddr obj) val))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; thunk related procedure
(define (delay-it type exp env) (make-thunk type exp env))
(define (actual-value exp env) (force-it (g-eval exp env)))

(define (force-it obj)
  (if (thunk? obj)
      (cond ((equal? t-type-l (thunk-type obj)) (actual-value (thunk-exp obj) (thunk-env obj)))
            ((equal? t-type-m (thunk-type obj)) (force-it-mem obj))
            (else (error "[GZ-env] thunk type error -- FORCE" (thunk-type obj))))
      obj))

(define (force-it-mem obj)
  (if (null? (thunk-env obj))
      (thunk-exp obj) ;; already evaluated thunk
      (let ((result (actual-value (thunk-exp obj) (thunk-env obj))))
           (replace-exp obj result) ;; replace <exp> with its <value>
           (replace-env obj '())    ;; forget unneeded <env>, also become the mark to tell if thunk evaluated
           result)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; core procedure
;;
;; <define> don't need change, because it will finally eval into <lambda>
;; <lambda> don't need change, because it's just <make-procedure>
;; problem begin from <apply>, just like what we do in textbook
(define (eval-application exp env)
  (define (process-param def val)
    (if (pair? def)
        val
        (g-eval val env)))
  (let ((proc (actual-value (operator exp) env)))
       (if (compound-procedure? proc)
           (g-apply proc
                    (map process-param (procedure-parameters proc) (operands exp))
                    env)
           (g-apply proc (list-of-values (operands exp) env) env))))

;; new eval-if
(define (eval-if exp env)
  (if (true? (actual-value (if-predicate exp) env))
      (g-eval (if-consequent exp) env)
      (g-eval (if-alternative exp) env)))

;; new eval
(define (g-eval exp env)
  (cond ((self-evaluating? exp) exp)
        ((variable? exp) (lookup-variable-value exp env))
        ((quoted? exp) (text-of-quotation exp))
        ((assignment? exp) (eval-assignment exp env))
        ((definition? exp) (eval-definition exp env))
        ((if? exp) (eval-if exp env))
        ((lambda? exp) (make-procedure (lambda-parameters exp)
                                       (lambda-body exp)
                                       env))
        ((begin? exp) (eval-sequence (begin-actions exp) env))
        ((cond? exp) (g-eval (cond->if exp) env))
        ((application? exp) (eval-application exp env))
        (else (error "[GZ-env]Unknown expression type -- EVAL" exp))))

;; new apply inners
(define (list-of-arg-values exps env)
  (if (no-operands? exps)
      '()
      (cons (force-it (first-operand exps))
            (list-of-arg-values (rest-operands exps) env))))

(define (list-of-delayed-args params exps env)
  (if (no-operands? exps)
      '()
      (let ((p (car params))
            (val (first-operand exps)))
           (cons (if (pair? p)
                     (cond ((equal? (cadr p) t-type-l) (delay-it t-type-l val env))
                           ((equal? (cadr p) t-type-m) (delay-it t-type-m val env))
                           (else "[GZ-env] error lazy type -- LIST-DELAY" exps))
                     val)
                 (list-of-delayed-args (cdr params) (rest-operands exps) env)))))

(define (list-of-delayed-params params)
  (define (checker p) (if (pair? p) (car p) p))
  (map checker params))

;; new apply
(define (g-apply procedure arguments env)
  (cond ((primitive-procedure? procedure)
         (apply-primitive-procedure procedure (list-of-arg-values arguments env)))
        ((compound-procedure? procedure)
         (eval-sequence (procedure-body procedure)
                        (extend-environment
                          (list-of-delayed-params (procedure-parameters procedure))
                          (list-of-delayed-args (procedure-parameters procedure) arguments env)
                          (procedure-environment procedure))))
        (else (error "[GZ-env] Unknown procedure type --- APPLY" procedure))))

;; new driver-loop
(define input-prompt ";;; G-Eval input:")
(define output-prompt ";;; G-Eval value:")
(define (driver-loop)
  (prompt-for-input input-prompt)
  (let ((input (read)))
       (let ((output (actual-value input the-global-environment)))
            (announce-output output-prompt)
            (user-print output)))
  (driver-loop))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; test code
;; (define a-test 0)
;; (define b-test 10)
;; (define c-test 100)
;; (define (f a (b lazy) (c lazy-memo)) a)
;; (define (f a (b lazy) (c lazy-memo)) b)
;; (define (f a (b lazy) (c lazy-memo)) c)
;; (define (f a (b lazy) (c lazy-memo)) (list b b))
;; (define (f a (b lazy) (c lazy-memo)) (list c c))
;; (f (begin (set! a-test (+ a-test 1)) a-test)
;;    (begin (set! b-test (+ b-test 1)) b-test)
;;    (begin (set! c-test (+ c-test 1)) c-test))
