(load "1.2.5.gcd.scm")

(define (make-rat n d)
  (let ((a (abs n))
        (b (abs d))
        (g (gcd (abs n) (abs d))))
       (if (< (* n d) 0)
           (cons (- 0 (/ a g)) (/ b g))
           (cons (/ a g) (/ b g)))))
