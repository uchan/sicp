;;;;;;;;;;;;;;;;;;;
;; 3.3.3 table

(define (g-assoc key records)
  (cond ((null? records) false)
        ((equal? key (caar records)) (car records))
        (else (g-assoc key (cdr records)))))

(define (lookup key table)
  (let ((record (g-assoc key (cdr table))))
       (if record
           (cdr record)
           false)))

(define (insert! key value table)
  (let ((record (g-assoc key (cdr table))))
       (if record
           (set-cdr! record value)
           (set-cdr! table (cons (cons key value) (cdr table)))))
  'ok)

(define (make-table) (list '*table*))
