(load "ex2.70.encode.scm")

(define (gen-pair n)
  (if (= n 1) (list (list 1 1))
      (append (gen-pair (- n 1)) (list (list n (expt 2 (- n 1)))))))

(define (show-symbol n)
  (define (add-bits pairs tree result)
    (if (null? pairs) result
        (add-bits (cdr pairs) tree
                  (append result (list (append (car pairs) (encode (list (caar pairs)) tree)))))))
  (let ((pairs (gen-pair n)))
       (add-bits pairs (generate-huffman-tree pairs) '())))
