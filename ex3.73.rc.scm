(load "3.5.3.paradigm.scm")

(define (RC R C dt)
  (define (input i v0)
    (add-streams (scale-stream i R)
                 (integral (scale-stream i (/ 1 C)) v0 dt)))
  input)

;; test code
(display-line 'ex3.73)
(display-line (stream-head ((RC 5 1 0.5) ones 1) 10))
