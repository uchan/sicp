(load "3.5.4.delay.scm")

(define (solve-2nd f y0 dy0 dt)
  (define y (integral (delay dy) y0 dt))
  (define dy (integral (delay ddy) dy0 dt))
  (define ddy (stream-map-ext f dy y))
  y)

;; test code
(display-line "ex3.79")
(display-line (stream-ref (solve-2nd (lambda (x y) (+ (* 3 x) (* 2 y))) 1 0 0.001) 1000))
