(load "ex2.67.decode.scm")

;; text book
(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set) (adjoin-set x (cdr set))))))

(define (make-leaf-set pairs)
  (if (null? pairs) '()
      (let ((pair (car pairs)))
           (adjoin-set (make-leaf (car pair) (cadr pair))
                       (make-leaf-set (cdr pairs))))))
;; problem
(define (generate-huffman-tree pairs)
  (successive-merge (make-leaf-set pairs)))
(define sample-pairs '((A 4) (B 2) (C 1) (D 1)))
;; answer
(define (successive-merge leaf-set)
  (define (merge set)
    (if (= 1 (length set)) (car set)
        (merge (adjoin-set (make-code-tree (car set) (cadr set)) (cddr set)))))
  (merge leaf-set))
