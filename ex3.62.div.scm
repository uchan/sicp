(load "ex3.61.inverse.scm")

(define (div-series s1 s2)
  (mul-series s1 (inverse s2)))

(define tangent-series (div-series sine-series cosine-series))

(newline)(newline)(newline)(newline)
(display-line 'ex3.62)
(display-line (stream-head tangent-series 10))
