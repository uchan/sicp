(load "ex1.43.repeated.scm")

;; helper func
(define (avg3 a b c) (/ (+ a b c) 3))

;; accuracy
(define dx 0.0001)

;; begin
(define (smooth1 f) (lambda (x) (avg3 (f x) (f (- x dx)) (f (+ x dx)))))
(define (smooth f n) ((repeated smooth1 n) f))
