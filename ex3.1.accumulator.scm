(define (make-accumulator num)
  (lambda (inc-num) (begin (set! num (+ num inc-num)) num)))

(define A (make-accumulator 5))

