(define (double f)
  (lambda (x) (f (f x))))

(define (inc i) (+ i 1))
(define (t) (((double (double double)) inc) 5))
