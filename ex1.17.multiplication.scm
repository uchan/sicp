(define (double a) (* a 2))
(define (halve a) (/ a 2))

(define (multiplication a b)
    (cond ((= b 0) 0)
          ((even? b) (double (multiplication a (halve b))))
          (else (+ a (multiplication a (- b 1))))))
