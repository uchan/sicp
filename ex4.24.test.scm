(define basenum (quote 1 2 3 4 5 6 7 8 9 10))
(define (append x y)
  (if (null? x)
      y
      (cons (car x) (append (cdr x) y))))
(define (mul x y)
  (if (null? y)
      (quote)
      (append x (mul x (cdr y)))))
(define (pow x y)
  (if (null? y)
      x
      (mul x (pow x (cdr y)))))

(define test (pow basenum (quote 1 2 3)))
