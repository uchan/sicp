(load "3.5.3.paradigm.scm")

(define (integral delayed-integrand initial-value dt)
  (define int
    (cons-stream initial-value
                 (let ((integrand (force delayed-integrand)))
                      (add-streams (scale-stream integrand dt)
                                  int))))
  int)

(define (solve f y0 dt)
  ;;(define y (integral dy yo dt)) ;;Premature reference to reserved name: dy
  (define y (integral (delay dy) y0 dt))
  (define dy (stream-map f y))
  y)


;; test code
(display-line "3.5.4 delay")
(display-line (stream-ref (solve (lambda (y) y) 1 0.001) 1000))
