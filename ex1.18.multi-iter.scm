(define (double a) (* a 2))
(define (halve a) (/ a 2))

;;; f(n) = c + a*n
;;; even:
;;; f(n) = c + (double a)*(halve n)
;;; odd:
;;; f(n) = c+a + a*(n-1)
(define (multi-iter a b)
    (define (iter a n c)
            (cond ((= n 0) c)
                  ((even? n) (iter (double a) (halve n) c))
                  (else (iter a (- n 1) (+ c a)))))
    (iter a b 0))
