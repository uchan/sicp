(define (reverse lst)
  (define (iter lst out)
    (if (null? lst)
        out
        (iter (cdr lst) (cons (car lst) out))))
  (iter lst nil))
