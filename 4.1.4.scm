;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 4.1.4

; system apply
(define apply-in-underlying-scheme apply)

;; primitive procedure
(define (primitive-or a . b) (or a (primitive-or (car b) (cdr b))))
(define (primitive-and a . b) (and a (primitive-and (car b) (cdr b))))
(define (primitive-procedure? proc) (tagged-list? proc 'primitive))
(define (primitive-implementation proc) (cadr proc))
(define primitive-procedures
  (list (list 'car car) (list 'cdr cdr) (list 'cons cons)
        (list 'null? null?) (list 'equal? equal?)
        (list 'number? number?)(list 'string? string?)(list 'symbol? symbol?)
        (list '+ +) (list '- -) (list '* *) (list '/ /) (list '> >) (list '< <) (list '= =)
        (list '>= >=) (list '<= <=) (list 'sqrt sqrt) (list 'integer? integer?)
        (list 'remainder remainder) (list 'not not)
        (list 'or primitive-or) (list 'and primitive-and)
        (list 'abs abs) (list 'member member)
        (list 'list list) (list 'newline newline) (list 'display display)))
(define (primitive-procedure-names) (map car primitive-procedures))
(define (primitive-procedure-objects) (map (lambda (proc) (list 'primitive (cadr proc)))
                                           primitive-procedures))
(define (apply-primitive-procedure proc args)
  (apply-in-underlying-scheme (primitive-implementation proc) args))

;; environment
(define (setup-environment)
  (let ((initial-env (extend-environment (primitive-procedure-names)
                                         (primitive-procedure-objects)
                                         the-empty-environment)))
       (define-variable! 'true true initial-env)
       (define-variable! 'false false initial-env)
       initial-env))

(define the-global-environment (setup-environment))

;; loop
(define input-prompt ";;; M-Eval input:")
(define output-prompt ";;; M-Eval value:")

(define (driver-loop)
  (prompt-for-input input-prompt)
  (let ((input (read)))
       (let ((output (g-eval input the-global-environment)))
            (announce-output output-prompt)
            (user-print output)))
  (driver-loop))

(define (prompt-for-input string) (newline)(newline)(display string)(newline))
(define (announce-output string) (newline)(display string)(newline))
(define (user-print object)
  (if (compound-procedure? object)
      (display (list 'compound-procedure
                     (procedure-parameters object)
                     (procedure-body object)
                     '<procedure-env>))
      (display object)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; can/should be used as primitive-procedures
;; number? string? symbol? pair? eq? list = < > + - * /
;;
;; macro or something else
;; '<-quote '()<-nil (f ...)<-variable length arguments
;;
;; can be defined as libaray
;; not and or append let set-car! set-cdr! map
;; cadr caddr caadr cdadr cddr cdddr cadddr <- used in 4.1.*.scm
;; error
;;
;; system dependent
;; read newline display
;; system-apply
