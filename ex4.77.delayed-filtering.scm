(load "4.4.4.query-system.scm")

;; replace original functions

;; begin process data
(define data-base '(
(assert! (keya 1 1))
(assert! (keya 1 2))
(assert! (keya 2 1))
(assert! (keyb 1 2))
(assert! (keyb 2 1))
))

;; begin test code
(define test-flag true)
(define sys-read read)
(define test-code '(
(and (keya ?x ?y) (not (keyb ?x ?y)))
(and (not (keyb ?x ?y)) (keya ?x ?y))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
