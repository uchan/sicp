;; condition
(define (inc n) (+ n 1))

;;a)
(define (product term a next b)
  (if (> a b) 1
      (* (term a) (product term (next a) next b))))

(define (factorial n)
  (define (x i)
    (if (= 1 i) 2.0
        (+ 1 (y (- i 1)))))
  (define (y i)
    (if (= 1 i) 3.0
        (+ 1 (x (- i 1)))))
  (define (term i) (/ (x i) (y i)))
  (product term 1 inc n))


;; b)
(define (product term a next b)
  (define (iter a result)
    (if (> a b) result
        (iter (next a) (* result (term a)))))
  (iter a 1))
