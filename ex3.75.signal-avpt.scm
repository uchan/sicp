(load "3.5.3.paradigm.scm")

;; code not in book
(define (make-stream-from-list lst)
  (if (null? lst)
      the-empty-stream
      (cons-stream (car lst) (make-stream-from-list (cdr lst)))))
(define sense-data (make-stream-from-list '(1 2 1.5 1 0.5 -0.1 -2 -3 -2 -0.5 0.2 3 4)))

(define (sign-change-detector second first)
  (cond ((> (* second first) 0) 0)
        ((and (= first 0) (= second 0)) 0)
        ((or (and (= first 0) (> second 0))
             (and (> first 0) (= second 0))) 0)
        ((and (< first 0) (= second 0)) 1)
        ((and (= first 0) (< second 0)) -1)
        ((and (> first 0) (< second 0)) -1)
        ((and (< first 0) (> second 0)) 1)
        (else (display-line 'errer-in-sign-change-detector))))

;; code in book
(define (make-zero-crossings input-stream last-value last-avpt)
  (let ((new-avpt (/ (+ (stream-car input-stream) last-value) 2)))
       (cons-stream (sign-change-detector last-avpt new-avpt)
                    (make-zero-crossings (stream-cdr input-stream)
                                         (stream-car input-stream)
                                         new-avpt))))

;; test code
(define zero-crossings (make-zero-crossings sense-data 0 0))
(display-line 'ex3.75)
(display-line (stream-head zero-crossings 13))
