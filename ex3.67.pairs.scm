(load "3.5.3.paradigm.scm")

(define (pairs-ext s t)
  (cons-stream (list (stream-car s) (stream-car t))
               (interleave (interleave (stream-map (lambda (x) (list (stream-car s) x))
                                                   (stream-cdr t))
                                       (stream-map (lambda (x) (list x (stream-car t)))
                                                   (stream-cdr s)))
                           (pairs-ext (stream-cdr s) (stream-cdr t)))))

;; test code
(display-line (stream-head (pairs-ext integers integers) 50))
