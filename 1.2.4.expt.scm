;;; recursive
(define (expt-recu b n)
    (if (= n 0)
        1
        (* b (expt-recu b (- n 1)))))

;;; interation
(define (expt-iter b n)
    (define (iter b counter product)
        (if (= counter 0)
            product
            (iter b (- counter 1) (* b product))))
    (iter b n 1))

;;; using square
(define (square n) (* n n))
(define (fast-expt b n)
    (cond ((= n 0) 1)
          ((even? n) (square (fast-expt b (/ n 2))))
          (else (* b (fast-expt b (- n 1))))))
