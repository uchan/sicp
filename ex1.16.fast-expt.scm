(define (square n) (* n n))

;;; invariant: a*b^n
;;;
;;; 'n' is even: a*b^n = a*(b^2)^(n/2)
;;;     a -> a
;;;     b -> b^2
;;;     n -> n/2
;;;
;;; 'n' is odd: a*b^n = (a*b)*b^(n-1)
;;;     a -> a*b
;;;     b -> b
;;;     n -> n-1

(define (fast-expt-iter b n)
    (define (iter b n a)
        (cond ((= n 0) a)
              ((even? n) (iter (square b) (/ n 2) a))
              (else (iter b (- n 1) (* a b)))))
    (iter b n 1))
