(load "3.3.5.constrains.scm")

(define (c+ x y)
  (let ((z (make-connector)))
       (adder x y z)
       z))

(define (c* x y)
  (let ((z (make-connector)))
       (multiplier x y z)
       z))

(define (c/ x y)
  (let ((z (make-connector)))
       (multiplier y z x)
       z))

(define (cv number)
  (let ((c (make-connector)))
       (constant number c)
       c))

(define (cf-converter x)
  (c+ (c* (c/ (cv 9) (cv 5)) x) (cv 32)))

;; tester code
(define (cf-tester)
  (define new-c (make-connector))
  (define new-f (cf-converter new-c))
  (probe "new-c:" new-c)
  (probe "new-f:" new-f)
  (set-value! new-c 25 'tester)
  (forget-value! new-c 'tester)
  (set-value! new-f 212 'tester)
  'done)
