(load "4.1.1.scm")

;; common
(define (and? exp) (tagged-list? exp 'and))
(define (or? exp) (tagged-list? exp 'or))
(define (get-ao-actions exp) (cdr exp))

;; eval method
(define (eval-and seq env)
  (cond ((null? seq) true)
        ((true? (g-eval (car seq) env)) (eval-and (cdr seq) env))
        (else false)))

(define (eval-or seq env)
  (cond ((null? seq) false)
        ((true? (g-eval (car seq) env)) true)
        (else (eval-or (cdr seq) env))))

;; expend method
(define (expand-and seq)
  (if (null? seq)
      'true
      (make-if (car seq) (expand-and (cdr seq)) 'false)))

(define (expand-or seq)
  (if (null? seq)
      'false
      (make-if (car seq) 'true (expand-or (cdr seq)))))

;; final
(define (g-eval exp env)
  (cond ((self-evaluating? exp) exp)
        ((variable? exp) (lookup-variable-value exp env))
        ((quoted? exp) (text-of-quotation exp))
        ((assignment? exp) (eval-assignment exp env))
        ((definition? exp) (eval-definition exp env))
        ((if? exp) (eval-if exp env))
        ((and? exp) (test-and (get-ao-actions exp) env))
        ((or? exp) (test-or (get-ao-actions exp) env))
        ((lambda? exp) (make-procedure (lambda-parameters exp)
                                       (lambda-body exp)
                                       env))
        ((begin? exp) (eval-sequence (begin-actions exp) env))
        ((cond? exp) (g-eval (cond-if exp) env))
        ((application? exp) (g-apply (g-eval (operator exp) env)
                                     (list-of-values (operands exp) env)))
        (else (error "[M-env]Unknown expression type -- EVAL" exp))))

;; tester
(define test-and eval-and)
(define test-or eval-or)
;(define test-and (lambda (exp env) (g-eval (expand-and exp) env)))
;(define test-or (lambda (exp env) (g-eval (expand-or exp) env)))
(driver-loop)
