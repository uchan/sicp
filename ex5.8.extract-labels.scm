(load "5.2.1.machine-model.scm")

;; answer
(define (extract-labels text receive)
  (if (null? text)
      (receive '() '())
      (extract-labels (cdr text)
        (lambda (insts labels)
          (let ((next-inst (car text)))
               (if (symbol? next-inst)
                   (let ((val (assoc next-inst labels)))
                        (display "[dbg2]")(display next-inst)(newline)
                        (display "[dbg1]")(display val)(newline)
                        (if val
                            (error "[ex] label already existed" next-inst)
                            (receive insts (cons (make-label-entry next-inst insts)
                                                 labels))))
                   (receive (cons (make-instruction next-inst) insts)
                            labels)))))))

;; test
(define my-machine
  (make-machine
    '(a)
    (list (list '- -) (list '= =) (list '* *))
    '(start
        (goto (label here))
      here
        (assign a (const 3))
        (goto (label there))
      here
        (assign a (const 4))
        (goto (label there))
      there)
  )
)
;  setting input
(start my-machine)
(display "machine result:")
(display (get-register-contents my-machine 'a))
(newline)
