(load "3.5.3.paradigm.scm")

(define (ln-summands n)
  (cons-stream (/ 1.0 n) (stream-map - (ln-summands (+ n 1)))))
(define ln-stream (partial-sums (ln-summands 1)))

;; test code
(newline)(newline)
(display-line (stream-head ln-stream 10))
(display-line (stream-head (euler-transform ln-stream) 10))
(display-line (stream-head (accelerated-sequence euler-transform ln-stream) 10))
