(load "1.2.4.expt.scm")

(define (square x) (* x x))

;; original expmod
(define (origin-expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (origin-expmod base (/ exp 2) m)) m))
        (else
         (remainder (* base (origin-expmod base (- exp 1) m)) m))))

;; new expmod
(define (new-expmod base exp m)
  (remainder (fast-expt base exp) m))


;; tester
(define (tester)
  (display "original:")
  (call-original (real-time-clock))
  (display "new:")
  (call-new (real-time-clock)))

(define (call-original start-time)
  (origin-expmod 100000 100000 7)
  (display (- (real-time-clock) start-time)))

(define (call-new start-time)
  (new-expmod 100000 100000 7)
  (display (- (real-time-clock) start-time)))
