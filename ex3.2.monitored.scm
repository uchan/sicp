(define (make-monitored f)
  (define count 0)
  (define (call param)
    (if (eq? param 'how-many-calls?)
        count
        (begin (set! count (+ count 1)) (f param))))
  call)
