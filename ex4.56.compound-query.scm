(load "4.4.4.query-system.scm")
(load "4.4.1.database.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;a)
(and (supervisor ?name (Bitdiddle Ben))
     (address ?name ?where))
;b)
(and (salary (Bitdiddle Ben) ?ben-amount)
     (salary ?name ?amount)
     (lisp-value < ?amount ?ben-amount))
;c)
(and (job ?person ?work)
     (supervisor ?person ?boss)
     (not (job ?boss (computer . ?type))))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
