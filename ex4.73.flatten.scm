(load "4.4.4.query-system.scm")

;; from 3.5.3
(define (interleave s1 s2)
  (if (stream-null? s1)
      s2
      (cons-stream (stream-car s1) (interleave s2 (stream-cdr s1)))))

;; replace original functions
(define (flatten-stream stream)
  (if (stream-null? stream)
      the-empty-stream
      (interleave
        (stream-car stream)
        (flatten-stream (stream-cdr stream)))))

;; begin process data
(define data-base '(
(assert! (test 1 1))
(assert! (test 1 2))
(assert! (test 2 1))
(assert! (rule (test ?x ?y)
               (test ?y ?x)))
))

;; begin test code
(define test-flag true)
(define sys-read read)
(define test-code '(
(test ?a ?b) ;; normal: <test 1 1> <test 1 2> <test 2 1>, replaced: infinite loop, because rules are not delayed
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
