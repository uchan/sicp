(load "ex1.37.cont-frac.scm")

(define (e-2 k)
  (define (d i)
    (let ((n (remainder i 3)))
      (cond ((= i 2) 2)
            ((or (= n 0) (= n 1)) 1)
            (else (expt 2 n)))))
  (cont-frac-i (lambda (x) 1) d k))
