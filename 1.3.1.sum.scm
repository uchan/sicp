(define (cube x) (* x x x))

(define (sum f a next b)
  (if (> a b)
      0
      (+ (f a)
         (sum f (next a) next b))))

;; cube sum
(define (inc n) (+ 1 n))
(define (sum-cubes a b)
  (sum cube a inc b))

;; integer sum
(define (identity x) x)
(define (sum-integers a b)
  (sum identity a inc b))

;; pi sum
(define (pi-sum a b)
  (define (pi-f x)  (/ 1.0 (* x (+ x 2))))
  (define (pi-next) (+ x 4))
  (sum pi-f a pi-next b))

;; integral sum
(define (integral f a b dx)
  (define (add-dx x) (+ x dx))
  (* (sum f (+ a (/ dx 2.0)) add-dx b) dx))
