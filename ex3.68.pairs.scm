(load "3.5.3.paradigm.scm")

(define (pairs-test s t)
  (interleave
    (stream-map (lambda (x) (list (stream-car s) x)) t)
    (pairs-test (stream-cdr s) (stream-cdr t))))
