(load "4.4.4.query-system.scm")
(load "4.4.1.database.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;define
(assert! (rule (big-shot ?person)
               (and (job ?person (?dep . ?rest))
                    (or (not (supervisor ?person ?anyone))  ;; those who do not have supervisor, like big boss
                        (and (supervisor ?person ?boss)     ;; those who have supervisor in other department
                             (not (job ?boss (?dep . ?rest-boss))))))))
;test
(big-shot ?x)
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
