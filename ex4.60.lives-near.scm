(load "4.4.4.query-system.scm")
(load "4.4.1.database.scm")

; helper
(define (name<? name1 name2)
  (cond ((and (null? name1) (null? name2)) false)
        ((and (null? name2) (not (null? name1))) false)
        ((and (null? name1) (not (null? name2))) true)
        (else (let ((n1 (symbol->string (car name1)))
                    (n2 (symbol->string (car name2))))
                   (cond ((string=? n1 n2) (name<? (cdr name1) (cdr name2)))
                         ((string<? n1 n2) true)
                         (else false))))))

(define test-flag true)
(define sys-read read)
(define test-code '(
; text book
(assert!
  (rule (lives-near ?person-1 ?person-2)
        (and (address ?person-1 (?town . ?rest-1))
             (address ?person-2 (?town . ?rest-2))
             (not (same ?person-1 ?person-2)))))
(assert! (rule (same ?x ?x)))
; answer
(assert! (rule (lives-near-new ?person-1 ?person-2)
               (and (address ?person-1 (?town . ?rest-1))
                    (address ?person-2 (?town . ?rest-2))
                    (not (same ?person-1 ?person-2))
                    (lisp-value name<? ?person-1 ?person-2))))
; test
(lives-near ?x ?y)
(lives-near-new ?x ?y)
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
