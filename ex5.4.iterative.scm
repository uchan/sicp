(load "5.2.1.machine-model.scm")

;; answer
(define my-machine
  (make-machine
    '(b n counter product)
    (list (list '- -) (list '= =) (list '* *))
    '(
        (assign counter (reg n))
        (assign product (const 1))
        expt-iter
            (test (op =) (reg counter) (const 0))
            (branch (label expt-done))
            (assign counter (op -) (reg counter) (const 1))
            (assign product (op *) (reg b) (reg product))
            (goto (label expt-iter))
        expt-done)
  )
)
;  setting input
(set-register-contents! my-machine 'b 3)
(set-register-contents! my-machine 'n 3)
(start my-machine)
(display "machine result:")
(display (get-register-contents my-machine 'product))
(newline)
