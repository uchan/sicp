(load "4.4.4.query-system.scm")
(load "4.4.1.database.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;database
(assert! (meeting accounting (Monday 9am)))
(assert! (meeting administration (Monday 10am)))
(assert! (meeting computer (Wednesday 3pm)))
(assert! (meeting administration (Firday 1pm)))
(assert! (meeting whole-company (Wednesday 4pm)))
;answer
;a)
(meeting ?dep (Firday ?time))
;b)
(assert! (rule (meeting-time ?person ?day-and-time)
               (or (meeting whole-company ?day-and-time)
                   (and (job ?person (?dep . ?rest))
                        (meeting ?dep ?day-and-time)))))
;c)
(meeting-time (Hacker Alyssa P) (Wednesday ?time))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
