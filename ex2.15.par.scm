(load "ex2.12.percent.scm")

(define (par1 r1 r2)
  (div-interval (mul-interval r1 r2)
                (add-interval r1 r2)))

(define (par2 r1 r2)
  (let ((one (make-interval 1 1)))
       (div-interval one
                     (add-interval (div-interval one r1)
                                   (div-interval one r2)))))

(define (tester a b c d)
  (let ((A (make-interval a b))
        (B (make-interval c d)))
       (display "par1:")
       (display (par1 A B))
       (display "\npar2:")
       (display (par2 A B))
       (newline)))
