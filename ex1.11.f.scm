;;; recursive 
(define (f-recu n)
    (if (< n 3)
        n
        (+ (f-recu (- n 1))
           (* 2 (f-recu (- n 2)))
           (* 3 (f-recu (- n 3))))))

;;; iterative
(define (f-iter n)
    (define (iter a b c n)
        (if (= n 0)
            a
            (iter b c (+ c (* 2 b) (* 3 a)) (- n 1))))
    (iter 0 1 2 n))
