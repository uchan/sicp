(load "5.2.1.machine-model.scm")

;; answer
(define my-machine
  (make-machine
    '(a)
    (list (list '- -) (list '= =) (list '* *))
    '(start
        (goto (label here))
      here
        (assign a (const 3))
        (goto (label there))
      here
        (assign a (const 4))
        (goto (label there))
      there)
  )
)
;  setting input
(start my-machine)
(display "machine result:")
(display (get-register-contents my-machine 'a))
(newline)
