(load "4.1.1.scm")

;; cond (<exp> => <func>)
;; ==> let ((something <exp>)) (<func> <exp>)
;; or
;; ==> if <exp> (<func> <exp>) <next clause>
;;
;;    using if, because 1) let is in next ex. 2) using let still have problem
(define (expand-clauses clauses)
  (if (null? clauses)
      'false
      (let ((first (car clauses)) (rest (cdr clauses)))
           (if (cond-else-clause? first)
               (if (null? rest)
                   (sequence->exp (cond-actions first))
                   (error "[M-env]ELSE clause isn't last -- COND->IF" clauses))
               (if (eq? (cadr first) '=>)
                   (make-if (car first)
                            (list (caddr first) (car first))
                            (expand-clauses rest))
                   (make-if (cond-predicate first)
                            (sequence->exp (cond-actions first))
                            (expand-clauses rest)))))))


;; tester
(driver-loop)
