(load "4.3.3.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;; pre-defined
(define (require p) (if (not p) (amb)))
(define (abs n) (if (> n 0) n (* -1 n)))
;; from ex4.35
(define (an-integer-between low high)
  (require (or (= low high) (< low high)))
  (amb low (an-integer-between (+ low 1) high)))
;; begin test
(define (safe? k others i)
  (cond ((null? others) true)
        ((= k (car others)) false)
        ((= i (abs (- k (car others)))) false)
        (else (safe? k (cdr others) (+ i 1)))))
(define (make-queens n i)
  (if (= 0 i)
      (quote)
      (let ((q (an-integer-between 1 n))
            (rest (make-queens n (- i 1))))
           (require (safe? q rest 1))
           (cons q rest))))
(make-queens 6 6)
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (begin (let ((input (car test-code)))
                  (set! test-code (cdr test-code))
                  (display input)(newline)
                  input))
      (sys-read)))

(driver-loop)
