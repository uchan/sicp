(load "4.4.4.query-system.scm")

;; replace original functions
(define (disjoin disjuncts frame-stream)
  (if (empty-disjunction? disjuncts)
      the-empty-stream
      (stream-append-delayed
;      (interleave-delayed
        (qeval (first-disjunct disjuncts) frame-stream)
        (delay (disjoin (rest-disjuncts disjuncts) frame-stream)))))

;; begin process data
(define data-base '(
(assert! (rule (otest ?x) (test ?x ?y)))
))
(define (db-setter n)
  (if (< n 10)
      (begin (set! data-base (append data-base (list (list 'assert! (list 'test 1 n))))) (db-setter (+ n 1)))
      (display "done")))
(db-setter 2)
(set! data-base (append data-base '((assert! (test 2 2)))))

;; begin test code
(define test-flag true)
(define sys-read read)
(define test-code '(
;(or (or-test 1) (test ?y ?y))
(or (otest 1) (test ?y ?y))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
