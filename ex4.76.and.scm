(load "4.4.4.query-system.scm")

;; replace original functions
(define (merge frame1 frame2)
  (if (null? frame2)
      frame1
      (let ((binding (car frame2)))
           (let ((result (extend-if-consistent (binding-variable binding) (binding-value binding) frame1)))
                (if (eq? result 'failed)
                    '()
                    (merge result (cdr frame2)))))))
                
(define (merge-a-frame frame frame-stream)
  (cond ((empty-stream? frame-stream) the-empty-stream)
        ((null? frame) frame-stream)
        (else (let ((first-frame (stream-car frame-stream)))
                   (if (null? first-frame)
                       (singleton-stream frame)
                       (let ((result (merge frame first-frame)))
                            (if (null? result)
                            (merge-a-frame frame (stream-cdr frame-stream))
                            (cons-stream result (merge-a-frame frame (stream-cdr frame-stream))))))))))

(define (merge-frame-stream fs1 fs2)
  (stream-flatmap (lambda (frame) (merge-a-frame frame fs1)) fs2))

(define (conjoin-map conjuncts frame-stream)
  (if (empty-conjunction? conjuncts)
      the-empty-stream
      (cons (qeval (first-conjunct conjuncts) frame-stream)
            (conjoin-map (rest-conjuncts conjuncts) frame-stream))))

(define (new-conjoin conjuncts frame-stream)
  (let ((results (conjoin-map conjuncts frame-stream)))
       (define (loop-merge first rest)
         (cond ((empty-stream? first) the-empty-stream)
               ((empty-stream? rest) first)
               (else (loop-merge (merge-frame-stream first (car rest)) (cdr rest)))))
       (if (empty-stream? results)
           the-empty-stream
           (loop-merge (car results) (cdr results)))))

(put 'and 'qeval new-conjoin) ;; connect 'and and <new-conjoin>
;; begin process data
(define data-base '(
(assert! (keya 1 1))
(assert! (keya 1 2))
(assert! (keya 2 1))
(assert! (keyb 1 2))
(assert! (keyb 2 2))
))

;; begin test code
(define test-flag true)
(define sys-read read)
(define test-code '(
(and (keya ?x ?y) (keyb ?x ?y))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
