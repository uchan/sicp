(load "ex4.6.let.scm")

;; helper
(define (make-let pairs body) (cons 'let (cons pairs body)))

;; answer
(define (let*? exp) (tagged-list? exp 'let*))
(define (let*-pairs exp) (cadr exp))
(define (let*-body exp) (cddr exp))

(define (let*->nest-lets exp)
  (if (null? (cdr (let*-pairs exp)))
      (make-let (let*-pairs exp) (let*-body exp))
      (make-let (list (car (let*-pairs exp)))
                (list (let*->nest-lets (cons 'let*
                                             (cons (cdr (let*-pairs exp))
                                                   (let*-body exp))))))))

;; g-eval
(define (g-eval exp env)
  (cond ((self-evaluating? exp) exp)
        ((variable? exp) (lookup-variable-value exp env))
        ((quoted? exp) (text-of-quotation exp))
        ((assignment? exp) (eval-assignment exp env))
        ((definition? exp) (eval-definition exp env))
        ((if? exp) (eval-if exp env))
        ((lambda? exp) (make-procedure (lambda-parameters exp)
                                       (lambda-body exp)
                                       env))
        ((begin? exp) (eval-sequence (begin-actions exp) env))
        ((cond? exp) (g-eval (cond->if exp) env))
        ((let? exp) (g-eval (let->combination exp) env))
        ((let*? exp) (g-eval (let*->nest-lets exp) env))
        ((application? exp) (g-apply (g-eval (operator exp) env)
                                     (list-of-values (operands exp) env)))
        (else (error "[M-env]Unknown expression type -- EVAL" exp))))

;; tester
(driver-loop)
