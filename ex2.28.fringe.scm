(define (gappend lst1 lst2)
  (display "append:")(display lst1)(display ",")(display lst2)(newline)
  (append lst1 lst2))

(define (show input result)
  (display "find:")(display input)(display ",")(display result)(newline))

(define (fringe lst)
  (define (find-leaf input result)
    (show input result)
    (cond ((null? input) result)
          ((not (pair? input)) (gappend result (list input)))
          (else (find-leaf (cdr input) (find-leaf (car input) result)))))
  (find-leaf lst '()))
