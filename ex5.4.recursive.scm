(load "5.2.1.machine-model.scm")

;; answer
(define my-machine
  (make-machine
    '(b n continue ret)
    (list (list '- -) (list '= =) (list '* *))
    '(
        (assign continue (label expt-done))
        expt-loop
            (test (op =) (reg n) (const 0))
            (branch (label base-case))
            (save continue)
            (assign n (op -) (reg n) (const 1))
            (assign continue (label after-expt))
            (goto (label expt-loop))
        after-expt
            (restore continue)
            (assign ret (op *) (reg b) (reg ret))
            (goto (reg continue))
        base-case
            (assign ret (const 1))
            (goto (reg continue))
        expt-done)
  )
)
;  setting input
(set-register-contents! my-machine 'b 3)
(set-register-contents! my-machine 'n 3)
(start my-machine)
(display "machine result:")
(display (get-register-contents my-machine 'ret))
(newline)
