(load "4.2.2.thunk.scm")

;; test code
(define (run-code code)
  (announce-output "[Ex4.34] code test begin")
  (user-print (actual-value code the-global-environment)))

(define test-code
'(begin
  (define (atom? o) (if (number? o) true
                    (if (string? o) true
                    (if (symbol? o) true
                    (if (null? o) true
                    (if (equal? o true) true
                    (if (equal? o false) true
                    false)))))))
  (define (cons x y) (lambda (m) (m x y)))
  (define (car z) (z (lambda (p q) p)))
  (define (cdr z) (z (lambda (p q) q)))
  (define show-count 0)
  (define show-max 100)
  (define show-too-long "<too long ...>")
  (define (show lst) (set! show-count 0)(show-car lst) (newline) "")
  (define (show-car lst)
    (if (> show-count show-max)
        (display show-too-long)
        (if (atom? lst)
            (begin (set! show-count (+ show-count 1))
                   (display lst) (display " "))
            (begin (set! show-count (+ show-count 1))
                   (display "( ") (show-car (car lst)) (show-cdr (cdr lst))))))
  (define (show-cdr lst)
    (if (> show-count show-max)
        (display show-too-long)
        (if (null? lst)
            (begin (set! show-count (+ show-count 1)) (display ") ") "")
            (if (atom? lst)
                (begin (set! show-count (+ show-count 3))
                       (display ". ") (display lst) (display " ) ") "")
                (begin (show-car (car lst)) (show-cdr (cdr lst)))))))
  (show (cons 1 2))
  (show (cons 1 (cons 2 (quote))))
  (show (cons (cons 1 (cons 2 3)) (cons 4 (cons 5 6))))
  (define ones (cons 1 ones))
  (show ones)
))

(run-code test-code)
