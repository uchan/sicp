(load "3.5.4.delay.scm")

(define (RLC R L C dt)
  (define (calc vc0 il0)
    (define vc (integral (delay dvc) vc0 dt))
    (define dvc (scale-stream il (/ -1 C)))
    (define il (integral (delay dil) il0 dt))
    (define dil (add-streams (scale-stream il (/ (- 0 R) L))
                             (scale-stream vc (/ 1 L))))
    (define (make-pairs s1 s2)
      (cons-stream (cons (stream-car s1) (stream-car s2))
                   (make-pairs (stream-cdr s1) (stream-cdr s2))))
    (make-pairs vc il))
  calc)

;; test code
(display-line "ex3.80")
(display-line (stream-ref ((calc 1 1 0.2 0.1) 10 0) 1000))
