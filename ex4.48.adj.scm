(load "4.3.3.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;; pre-defined
(define (require p) (if (not p) (amb)))
(define (memq e lst)
  (cond ((null? lst) false)
        ((equal? e (car lst)) true)
        (else (memq e (cdr lst)))))
;; basic definition
(define nouns (quote noun student professor cat class))
(define verbs (quote verb studies lectures eats sleeps))
(define articles (quote article the a))
(define prepositions (quote prep for to in by with))
(define adjectives (quote adj good big happy))
;; basic functions
(define (parse-sentence)
  (list "sentence"
        (parse-noun-phrase)
        (parse-verb-phrase)))
(define (parse-prepositional-phrase)
  (list "prep-phrase"
        (parse-word prepositions)
        (parse-noun-phrase)))
;  verb
(define (parse-verb-phrase)
  (maybe-extend-verb (parse-word verbs)))
(define (maybe-extend-verb verb-phrase)
  (amb verb-phrase
       (maybe-extend-verb (list "verb-phrase" verb-phrase
                                (parse-prepositional-phrase)))))
;  noun
(define (parse-adj-noun-phrase)
  (list "adj-noun-phrase"
        (parse-word adjectives)
        (parse-word nouns)))
(define (parse-simple-noun-phrase)
  (list "simple-noun-phrase"
        (parse-word articles)
        (amb (parse-word nouns) (parse-adj-noun-phrase))))
(define (parse-noun-phrase)
  (maybe-extend-noun (parse-simple-noun-phrase)))
(define (maybe-extend-noun noun-phrase)
  (amb noun-phrase
       (maybe-extend-noun (list "noun-phrase" noun-phrase
                                (parse-prepositional-phrase)))))
;  word
(define (parse-word word-list)
  (require (not (null? *unparsed* )))
  (require (memq (car *unparsed*) (cdr word-list)))
  (let ((found-word (car *unparsed*)))
       (set! *unparsed* (cdr *unparsed*))
       (list (car word-list) found-word)))
;; runtime define
(define *unparsed* (quote))
(define (parse input)
  (set! *unparsed* input)
  (let ((sent (parse-sentence)))
       (require (null? *unparsed*))
       sent))
;; test code
(parse (quote the cat eats))
(parse (quote the student with the cat sleeps in the class))
(parse (quote the professor lectures to the student with the cat))
(parse (quote the happy professor lectures to the good student with the cat))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (begin (let ((input (car test-code)))
                  (set! test-code (cdr test-code))
                  (display input)(newline)
                  input))
      (sys-read)))

(driver-loop)
