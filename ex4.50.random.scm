(load "4.3.3.scm")

;;=================================================
;; answer code
;;=================================================
;  helper
(define (pick-random lst left)
  (cond ((null? (cdr lst)) (cons (car lst) left))
        ((= 1 (random 2)) (cons (car lst) (append left (cdr lst))))
        (else (pick-random (cdr lst) (append left (list (car lst)))))))
;  main
(define (analyze-amb exp)
  (let ((cprocs (map analyze (amb-choices exp))))
       (lambda (env succeed fail)
         (define (try-next choices)
           (if (null? choices)
               (fail)
               (let ((result (pick-random choices '())))
                    ((car result) env succeed (lambda () (try-next (cdr result)))))))
         (try-next cprocs))))

;;=================================================
;; test code
;;=================================================
(define test-flag true)
(define sys-read read)
(define test-code '(
(list (amb 1 2 3 4 5 6 7 8 9 10) (amb "a" "b" "c"))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (begin (let ((input (car test-code)))
                  (set! test-code (cdr test-code))
                  (display input)(newline)
                  input))
      (sys-read)))

(driver-loop)
