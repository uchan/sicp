;;;;;;;;;;;;;;;;;;;
;; my fib define
(define (my-fib n)
    (define (iter a b n)
        (cond ((= n 0) a)
              ((= n 1) b)
              (else (iter b (+ a b) (- n 1)))))
    (iter 0 1 n))


;;;;;;;;;;;;;;;;;;;
;; book fib define
(define (fib n)
    (fib-iter 1 0 n))
(define (fib-iter a b count)
    (if (= count 0)
        b
        (fib-iter (+ a b) a (- count 1))))
