(load "3.3.4.circuits.scm")

(define (ripple-carry-adder A B S C)
  (let ((cout (make-wire)))
       (full-adder (car A) (car B) C (car S) cout)
       (if (null? (cdr A))
           (set-signal! cout 0)
           (ripple-carry-adder (cdr A) (cdr B) (cdr S) cout))))

;; tester
(define (make-wires n)
  (if (= 0 n)
      '()
      (cons (make-wire) (make-wires (- n 1)))))
(define (setter wires values)
  (if (null? wires)
      'done
      (begin (set-signal! (car wires) (car values))
             (setter (cdr wires) (cdr values)))))
(define (getter wires)
  (if (null? wires)
      'done
      (begin (display "value:")
             (display (get-signal (car wires)))
             (newline)
             (getter (cdr wires)))))

(define an (make-wires 3))
(define bn (make-wires 3))
(define sn (make-wires 3))
(define c0 (make-wire))
(define nfa (ripple-carry-adder an bn sn c0))

;; monitor
(probe 's1 (car sn))
(probe 's2 (cadr sn))
(probe 's3 (caddr sn))
