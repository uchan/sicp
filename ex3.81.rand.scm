(load "3.5.5.monte-carlo.scm")

(define (rand-gen input-stream)
  (define (make-rand init-value)
    (define numbers
      (cons-stream init-value
                   (stream-map rand-update numbers)))
    numbers)
  (define (make-num-stream num-stream rest-commands)
    (cons-stream (stream-car num-stream)
                 (process (stream-cdr num-stream) rest-commands)))
  (define (process numbers commands)
    (cond ((eq? (stream-car commands) 'gen)
           (make-num-stream numbers (stream-cdr commands)))
          ((and (pair? (stream-car commands)) (eq? (car (stream-car commands)) 'reset)
                (number? (cdr (stream-car commands))))
           (make-num-stream (make-rand (cdr (stream-car commands))) (stream-cdr commands)))
          (else (error "bad command : " (stream-car commands)))))
  (process (make-rand random-init) input-stream))


;; test code
(define (make-stream-from-list lst)
  (if (null? lst)
      the-empty-stream
      (cons-stream (car lst) (make-stream-from-list (cdr lst)))))
(define commands1 '(gen (reset . 10) gen gen (reset . 1) gen))
(define commands2 '((reset . 10) gen gen (reset . 1) gen))

(display-line "ex3.81")
(display-line "commands1")
(display-line (stream-head (make-stream-from-list commands1) 6))
(display-line (stream-head (rand-gen (make-stream-from-list commands1)) 6))
(display-line "commands2")
(display-line (stream-head (make-stream-from-list commands2) 5))
(display-line (stream-head (rand-gen (make-stream-from-list commands2)) 5))
