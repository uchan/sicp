(load "5.2.1.machine-model.scm")

;; a)
(define ct-machine
  (make-machine
    '(tree val continue t)
    (list (list 'null? null?) (list 'pair? pair?) (list 'car car) (list 'cdr cdr) (list '+ +))
    '(controller
        (assign continue (label ct-done))
      ct-loop
        (test (op null?) (reg tree))
        (branch (label null-case))
        (test (op pair?) (reg tree))
        (branch (label else-case))
        ;; base case 2: atom
        (assign val (const 1))
        (goto (reg continue))
      null-case
        ;; base case 1: null
        (assign val (const 0))
        (goto (reg continue))
      else-case
        (save continue)
        (save tree)
        ;; set for loop: car
        (assign tree (op car) (reg tree))
        (assign continue (label after-car))
        (goto (label ct-loop))
      after-car
        ;; set for loop: cdr
        (restore tree)
        (save val)      ;; val=(counter-leave (car tree))
        (assign tree (op cdr) (reg tree))
        (assign continue (label after-cdr))
        (goto (label ct-loop))
      after-cdr
        (restore t) ;; get last val=(counter-leave (car tree))
        (assign val (op +) (reg t) (reg val))
        (restore continue)
        (goto (reg continue))
      ct-done)))
;  setting input
(newline)
(set-register-contents! ct-machine 'tree '(1 (2 3 (4))))
(start ct-machine)
(display "ct-machine result:")
(display (get-register-contents ct-machine 'val))
(newline)
