(load "ex3.50.stream-map.scm")
(load "3.5.3.paradigm.scm")

;; code not in book
(define (make-stream-from-list lst)
  (if (null? lst)
      the-empty-stream
      (cons-stream (car lst) (make-stream-from-list (cdr lst)))))
(define sense-data (make-stream-from-list '(1 2 1.5 1 0.5 -0.1 -2 -3 -2 -0.5 0.2 3 4)))

(define (sign-change-detector second first)
  (cond ((> (* second first) 0) 0)
        ((and (= first 0) (= second 0)) 0)
        ((or (and (= first 0) (> second 0))
             (and (> first 0) (= second 0))) 0)
        ((and (< first 0) (= second 0)) 1)
        ((and (= first 0) (< second 0)) -1)
        ((and (> first 0) (< second 0)) -1)
        ((and (< first 0) (> second 0)) 1)
        (else (display-line 'errer-in-sign-change-detector))))

;; code in book
(define (make-zero-crossings input-stream last-value)
  (cons-stream
    (sign-change-detector (stream-car input-stream) last-value)
    (make-zero-crossings (stream-cdr input-stream) (stream-car input-stream))))

(define zero-crossings (make-zero-crossings sense-data 0))

(define zero-crossings2
  (stream-map-ext sign-change-detector sense-data (cons-stream 0 sense-data)))

