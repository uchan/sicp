(load "3.5.2.infinite.scm")

(define (mul-streams s1 s2) (stream-map-ext * s1 s2))

(define factorials (cons-stream 1 (mul-streams (stream-cdr integers) factorials)))
