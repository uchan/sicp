(load "5.2.1.machine-model.scm")

;; sample 1
(define gcd-machine
  (make-machine
    '(a b t)
    (list (list 'rem remainder) (list '= =))
    '(test-b
        (test (op =) (reg b) (const 0))
        (branch (label gcd-done))
        (assign t (op rem) (reg a) (reg b))
        (assign a (reg b))
        (assign b (reg t))
        (goto (label test-b))
      gcd-done)))
;  setting input
(set-register-contents! gcd-machine 'a 206)
(set-register-contents! gcd-machine 'b 40)
(start gcd-machine)
(display "gcd-machine result:")
(display (get-register-contents gcd-machine 'a))
(newline)

;; sample 2
(define gcd-machine-print
  (make-machine
    '(a b t)
    (list (list 'rem remainder) (list '= =) (list 'print display) (list 'read read))
    '(gcd-loop
        (assign a (op read))
        (assign b (op read))
      test-b
        (test (op =) (reg b) (const 0))
        (branch (label gcd-done))
        (assign t (op rem) (reg a) (reg b))
        (assign a (reg b))
        (assign b (reg t))
        (goto (label test-b))
      gcd-done
        (perform (op print) (reg a))
        (goto (label gcd-loop)))))
(start gcd-machine-print)
