(load "4.4.4.query-system.scm")
(load "4.4.1.database.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;; database
(assert! (son Adam Cain))
(assert! (son Cain Enoch))
(assert! (son Enoch Irad))
(assert! (son Irad Mehujael))
(assert! (son Mehujael Methushael))
(assert! (son Methushael Lamech))
(assert! (wife Lamech Ada))
(assert! (son Ada Jabal))
(assert! (son Ada Jubal))
;; (son Adam Cain) ==> Adam's son is Cain
;; answer
(assert! (rule (grandson ?G ?S)
               (and (son ?F ?S)
                    (son ?G ?F))))
(assert! (rule (son ?M ?S)
               (and (wife ?M ?W)
                    (son ?W ?S))))

;; query
(grandson Cain ?ans1)
(son Lamech ?ans2)
(grandson Methushael ?ans3)
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
