;;text book
(define (enumerate-interval low high)
  (if (> low high) '()
      (cons low (enumerate-interval (+ low 1) high))))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))

;; answer
(define (tri-seq n)
  (flatmap (lambda (a)
             (flatmap (lambda (b)
                        (map (lambda (c) (list a b c))
                             (enumerate-interval 1 (- b 1))))
                      (enumerate-interval 1 (- a 1))))
           (enumerate-interval 1 n)))

(define (tri-sum n s)
  (define (sum? seq) (= (accumulate + 0 seq) s))
  (filter sum? (tri-seq n)))
