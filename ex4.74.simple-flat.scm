(load "4.4.4.query-system.scm")

;; answer a)
(define (simple-stream-flatmap proc s)
  (simple-flatten (stream-map proc s)))
(define (simple-flatten stream)
  (stream-map stream-car (stream-filter (lambda (s) (not (stream-null? s))) stream)))

;; answer b)
; behavior not changed:
; negate: the <proc> is (if xxx (singletonxx) empty) so element of <s> should be [empty] or [singleton]
; lisp-vale: same as negate
; find-assertions: <proc> called <check-an-assertion> which has same (if xxx (singletonxx) empty)

;; replace original functions
(define (negate operands frame-stream)
  (simple-stream-flatmap (lambda (frame)
                           (if (stream-null? (qeval (negated-query operands) (singleton-stream frame)))
                               (singleton-stream frame)
                               the-empty-stream))
                         frame-stream))
(put 'not 'qeval negate) ;; connect 'not and <negate>
(define (lisp-value call frame-stream)
  (simple-stream-flatmap (lambda (frame)
                           (if (execute (instantiate call frame
                                                     (lambda (v f) (error "Unknown pat var -- LISP-VALUE" v))))
                               (singleton-stream frame)
                               the-empty-stream))
                         frame-stream))
(put 'lisp-value 'qeval lisp-value) ;; connect 'lisp-value and <lisp-value>
(define (find-assertions pattern frame)
  (simple-stream-flatmap (lambda (datum) (check-an-assertion datum pattern frame))
                         (fetch-assertions pattern frame)))

;; begin process data
(define data-base '(
(assert! (test 1 1))
(assert! (test 1 2))
(assert! (test 2 1))
))

;; begin test code
(define test-flag true)
(define sys-read read)
(define test-code '(
(test 1 ?x)
(assert! (rule (same ?a ?a)))
(and (test ?x ?y) (not (same 2 ?x)))
(and (test ?x ?y) (lisp-value > ?x 1))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
