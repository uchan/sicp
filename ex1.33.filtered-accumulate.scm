;; iter version
(define (filtered-accumulate filter combiner null-value term a next b)
  (define (iter a result)
    (if (> a b) result
        (if (filter a)
            (iter (next a) (combiner result (term a)))
            (iter (next a) result))))
  (iter a null-value))

;; a)
(load "1.2.6.prime.scm")
(define (inc n) (+ n 1))
(define (identity x) x)
(define (sum-prime a b)
  (filtered-accumulate prime? + 0 identity a inc b))


;; b)
(load "1.2.5.gcd.scm")
(define (sum-gcd n)
  (define (gcd-filter i)
    (= 1 (gcd i n)))
  (filtered-accumulate gcd-filter * 1 identity 1 inc n))
