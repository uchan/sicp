(load "4.3.3.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;; pre-defined
(define (require p) (if (not p) (amb)))
(define (an-integer-staring-from n)
  (amb n (an-integer-staring-from (+ n 1))))
;; ex4.35
(define (an-integer-between low high)
  (require (or (= low high) (< low high)))
  (amb low (an-integer-between (+ low 1) high)))
;; ex4.37
(define (a-pythagorean-triple-between low high)
  (let ((i (an-integer-between low high))
        (hsq (* high high)))
       (let ((j (an-integer-between i high)))
            (let ((ksq (+ (* i i) (* j j))))
                 (require (or (> hsq ksq) (= hsq ksq)))
                 (let ((k (sqrt ksq)))
                      (require (integer? k))
                      (list i j k))))))
(a-pythagorean-triple-between 3 20)
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (begin (let ((input (car test-code)))
                  (set! test-code (cdr test-code))
                  (display input)(newline)
                  input))
      (sys-read)))

(driver-loop)
