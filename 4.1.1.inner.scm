;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 4.1.1 inner
(define (list-of-values exps env)
  (if (no-operands? exps)
      '()
      (cons (g-eval (first-operand exps) env)
            (list-of-values (rest-operands exps) env))))

(define (eval-if exp env)
  (if (true? (g-eval (if-predicate exp) env))
      (g-eval (if-consequent exp) env)
      (g-eval (if-alternative exp) env)))

(define (eval-sequence exps env)
  (cond ((last-exp? exps) (g-eval (first-exp exps) env))
        (else (g-eval (first-exp exps) env)
              (eval-sequence (rest-exps exps) env))))

(define (eval-assignment exp env)
  (set-variable-value! (assignment-variable exp)
                       (g-eval (assignment-value exp) env)
                       env)
  'ok)

(define (eval-definition exp env)
  (define-variable! (definition-variable exp)
                    (g-eval (definition-value exp) env)
                    env)
  'ok)
