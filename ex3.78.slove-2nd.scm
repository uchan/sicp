(load "3.5.4.delay.scm")

(define (solve-2nd a b y0 dy0 dt)
  (define y (integral (delay dy) y0 dt))
  (define dy (integral (delay ddy) dy0 dt))
  (define ddy (add-streams (scale-stream dy a)
                           (scale-stream y b)))
  y)

;; test code
(display-line "ex3.78")
(display-line (stream-ref (solve-2nd 3 2 1 0 0.001) 1000))
