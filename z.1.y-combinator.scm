;; original
(define fact-orig
  (lambda (n)
    (if (zero? n)
        1
        (* n (fact-orig (- n 1))))))

;; step2
;; sample of (fact 5) call:
(((lambda (procedure)
    (lambda (n)
      (if (zero? n)
          1
          (* n ((procedure procedure) (- n 1))))))
  (lambda (procedure)
    (lambda (n)
      (if (zero? n)
          1
          (* n ((procedure procedure) (- n 1)))))))
 5)

;; step 3, rewrite fact, leave (procedure procedure) out
(define F
  ((lambda (func-arg)
     (lambda (n)
       (if (zero? n)
           1
           (* n (func-arg (- n 1))))))
   (lambda (arg) ((procedure procedure) arg))))

;; step 4, combine with step2 <-- write same thing twice
;; and take out the same parts
(define F*
  (lambda (func-arg)
    (if (zero? n)
        1
        (* n (func-arg) (- n 1)))))

(define fact-4
  ((lambda (procedure)
     (F* (lambda (arg) ((procedure procedure) arg))))
   (lambda (procedure)
     (F* (lambda (arg) ((procedure procedure) arg))))))

;; step 5, make F* into argument, so we only need F* once
(define Y
  (lambda (X)
    ((lambda (procedure)
       (X (lambda (arg) ((procedure procedure) arg))))
     (lambda (procedure)
       (X (lambda (arg) ((procedure procedure) arg)))))))

(define fact-5 (Y F*))

;; step extra: make lambda in (lambda (X)) into once
(define testF
  (lambda (procedure)
    (X (lambda (arg) ((procedure procedure) arg)))))
;; Y --> (testF testF)
