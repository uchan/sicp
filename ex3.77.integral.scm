(load "3.5.4.delay.scm")

(define (integral delayed-integrand initial-value dt)
  (cons-stream initial-value
               (let ((integrand (force delayed-integrand)))
                    (integral (delay (stream-cdr integrand))
                              (+ (* dt (stream-car integrand)) initial-value)
                              dt))))

;; test code
(display-line "ex3.77")
(display-line (stream-ref (solve (lambda (y) y) 1 0.001) 1000))
