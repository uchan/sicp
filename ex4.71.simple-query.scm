(load "4.4.4.query-system.scm")

;; replace original functions
(define (simple-query query-pattern frame-stream)
  (stream-flatmap (lambda (frame)
                    (stream-append (find-assertions query-pattern frame)
                                   (apply-rules query-pattern frame)))
                  frame-stream))

(define (disjoin disjuncts frame-stream)
  (if (empty-disjunction? disjuncts)
      the-empty-stream
      (interleave
        (qeval (first-disjunct disjuncts) frame-stream)
        (disjoin (rest-disjuncts disjuncts) frame-stream))))

;; begin process data
(define data-base '(
(assert! (test 1 1))
(assert! (test 1 2))
(assert! (test 2 1))
(assert! (rule (test ?x ?y)
               (test ?y ?x)))
))

;; begin test code
(define test-flag true)
(define sys-read read)
(define test-code '(
(test ?a ?b) ;; normal: <test 1 1> <test 1 2> <test 2 1>, replaced: infinite loop, because rules are not delayed
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
