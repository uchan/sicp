(load "4.3.3.scm")

;;========================================================
;; answer code
;;========================================================
;; for 'require' symbol
(define (require? exp) (tagged-list? exp 'require))
(define (require-predicate exp) (cadr exp))

;-------------------------------------
; main answer
(define (analyze-require exp)
  (let ((pproc (analyze (require-predicate exp))))
       (lambda (env succeed fail)
         (pproc env
                (lambda (pred-value fail2)
                  (if (not (true? pred-value))
                      (fail2)
                      (succeed 'ok fail2)))
                fail))))

;-------------------------------------
;; main function
(define (analyze exp)
  (cond ((self-evaluating? exp) (analyze-self-evaluating exp))
        ((quoted? exp) (analyze-quoted exp))
        ((variable? exp) (analyze-variable exp))
        ((assignment? exp) (analyze-assignment exp))
        ((definition? exp) (analyze-definition exp))
        ((if? exp) (analyze-if exp))
        ((not? exp) (analyze (not->if (get-aon-action exp))))
        ((or? exp) (analyze (or->if (get-aon-action exp))))
        ((and? exp) (analyze (and->if (get-aon-action exp))))
        ((lambda? exp) (analyze-lambda exp))
        ((begin? exp) (analyze-sequence (begin-actions exp)))
        ((cond? exp) (analyze (cond->if exp)))
        ((let? exp) (analyze-let exp))
        ((amb? exp) (analyze-amb exp))
        ((require? exp) (analyze-require exp))
        ((application? exp) (analyze-application exp))
        (else (error "[M-amb] Unknow expression type -- ANALYZE" exp))))

;;========================================================
;; test code
;;========================================================
(define test-flag true)
(define sys-read read)
(define test-code '(
;(list (amb 1 2 3) (amb "a" "b"))
;; pre-defined
(define (square x) (* x x))
(define (divides? a b) (= (remainder b a) 0))
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))
(define (smallest-divisor n) (find-divisor n 2))
(define (prime? n) (= n (smallest-divisor n)))
;; begin test
;(define (require p) (if (not p) (amb)))
(define (an-element-of items)
  (require (not (null? items)))
  (amb (car items) (an-element-of (cdr items))))
(define (prime-sum-pair list1 list2)
  (let ((a (an-element-of list1))
        (b (an-element-of list2)))
       (require (prime? (+ a b)))
       (list a b)))
(prime-sum-pair (quote 1 3 5 8) (quote 20 35 110))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (begin (let ((input (car test-code)))
                  (set! test-code (cdr test-code))
                  (display input)(newline)
                  input))
      (sys-read)))

(driver-loop)
