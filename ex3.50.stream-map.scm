(load "3.5.1.stream.scm")

(define (stream-map-ext proc . argstreams)
  (if (stream-null? (car argstreams))
      the-empty-stream
      (cons-stream
        (apply proc (map stream-car argstreams))
        (apply stream-map-ext (cons proc (map stream-cdr argstreams))))))

;; test
(define s1 (stream-enumerate-interval 1 100))
(define s2 (stream-enumerate-interval 101 200))
(display (stream-head s1 10))(newline)
(display (stream-head s2 20))(newline)
(display (stream-head (stream-map-ext + s1 s2) 10))(newline)
