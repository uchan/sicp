(load "3.5.1.stream.scm")

(define (integers-starting-from n)
  (cons-stream n (integers-starting-from (+ n 1))))
(define integers (integers-starting-from 1))

(define ones (cons-stream 1 ones))

(load "ex3.50.stream-map.scm")
(define (add-streams s1 s2) (stream-map-ext + s1 s2))

(define fibs (cons-stream 0 (cons-stream 1 (add-streams (stream-cdr fibs) fibs))))

(define (scale-stream stream factor)
  (stream-map (lambda (x) (* x factor)) stream))
