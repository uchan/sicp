(load "3.5.3.paradigm.scm")

(define (sqrt-stream-test x)
  (cons-stream 1.0 (stream-map (lambda (guess) (sqrt-improve guess x)) (sqrt-stream x))))

(define (testf f)
    (let ((start-time (real-time-clock)))
        (f)
        (- (real-time-clock) start-time)))

(display-line 'orignal-code)
(display-line (testf (lambda () (stream-ref (sqrt-stream 2) 100000))))
(display-line 'new-code)
(display-line (testf (lambda () (stream-ref (sqrt-stream-test 2) 100000))))
