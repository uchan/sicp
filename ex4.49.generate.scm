(load "4.3.3.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;; pre-defined
(define (require p) (if (not p) (amb)))
(define (memq e lst)
  (cond ((null? lst) false)
        ((equal? e (car lst)) true)
        (else (memq e (cdr lst)))))
;; basic definition
(define nouns (quote noun student professor cat class))
(define verbs (quote verb studies lectures eats sleeps))
(define articles (quote article the a))
(define prepositions (quote prep for to in by with))
;; basic functions
(define (parse-sentence)
  (list "sentence"
        (parse-noun-phrase)
        (parse-verb-phrase)))
(define (parse-prepositional-phrase)
  (list "prep-phrase"
        (parse-word prepositions)
        (parse-noun-phrase)))
;  verb
(define (parse-verb-phrase)
  (maybe-extend-verb (parse-word verbs)))
(define (maybe-extend-verb verb-phrase)
  (amb verb-phrase
       (maybe-extend-verb (list "verb-phrase" verb-phrase
                                (parse-prepositional-phrase)))))
;  noun
(define (parse-simple-noun-phrase)
  (list "simple-noun-phrase"
        (parse-word articles)
        (parse-word nouns)))
(define (parse-noun-phrase)
  (maybe-extend-noun (parse-simple-noun-phrase)))
(define (maybe-extend-noun noun-phrase)
  (amb noun-phrase
       (maybe-extend-noun (list "noun-phrase" noun-phrase
                                (parse-prepositional-phrase)))))
;  word
(define (pick lst)
  (require (not (null? lst)))
  (amb (car lst) (cdr lst)))
(define (parse-word word-list)
  (list (car word-list) (pick (cdr word-list))))
;; test code
(parse-sentence)
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (begin (let ((input (car test-code)))
                  (set! test-code (cdr test-code))
                  (display input)(newline)
                  input))
      (sys-read)))

(driver-loop)
