(load "ex3.70.merge.scm")

(define (weight-r pair)
  (define (tri x) (* x x x))
  (+ (tri (car pair)) (tri (cadr pair))))

(define r-num-pairs (weighted-pairs integers integers weight-r))

(define (r-num)
  (define (iter s)
    (let ((s0 (stream-ref s 0)) (s1 (stream-ref s 1)))
         (if (= (weight-r s0) (weight-r s1))
             (cons-stream (list (weight-r s0) s0 s1)
                          (iter (stream-cdr s)))
             (iter (stream-cdr s)))))
  (iter r-num-pairs))


;; test code
(display-line 'ex3.71)
(display-line (stream-head r-num-pairs 20))
(display-line '(1729 4104 13832 20683 32832 39312 40033 46683 64232 65728 110656 110808 134379 149389 165464 171288 195841 216027 216125 262656 314496 320264 327763))
(display-line (stream-head (r-num) 7))
