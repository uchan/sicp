(define (sqrt x)
	(sqrt-iter x 1.0))
(define (sqrt-iter x y)
	(if (sqrt-good-enough? x y)
	    y
	    (sqrt-iter x (average y (/ x y)))))
(define (sqrt-good-enough? x y)
	(< (abs (- (* y y) x)) 0.01))
(define (average a b)
	(/ (+ a b) 2))

