;;; calcuate p' q'
;;; 
;;; T-pq:
;;; 0:a,b
;;; 1:a1,b1
;;;   a1=bq+aq+ap
;;;   b1=bp+aq
;;; 2:a2,b2
;;;   a2=b1q+a1q+a1p
;;;     =(bp+aq)q+(bq+aq+ap)q+(bq+aq+ap)p
;;;     =bpq+aq^2 + bq^2+aq^2+apq + bpq+apq+ap^2
;;;     =2aq^2+bq^2+2bpq+2apq+ap^2
;;;     =bq^2+2bpq+2aq^2+2apq+ap^2
;;;     =b(q^2+2pq)+aq^2+2apq + aq^2+ap^2
;;;     =b(q^2+2pq)+a(q^2+2pq)+a(q^2+p^2)
;;;   b2=b1p+a1q
;;;     =(bp+aq)p+(bq+aq+ap)q
;;;     =bp^2+apq + bq^2+aq^2+apq
;;;     =b(p^2+q^2)+a(2pq+q^2)
;;; 
;;; so:
;;; q'=q^2+2pq
;;; p'=q^2+p^2
;;; 

(define (fib n)
    (fib-iter 1 0 0 1 n))
(define (fib-iter a b p q count)
    (cond ((= count 0) b)
          ((even? count)
           (fib-iter a
                     b
                     (+ (* q q) (* p p))    ; compute p'
                     (+ (* q q) (* 2 p q))  ; compute q'
                     (/ count 2)))
          (else (fib-iter (+ (* b q) (* a q) (* a p))
                          (+ (* b p) (* a q))
                          p
                          q
                          (- count 1)))))
