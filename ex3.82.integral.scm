(load "3.5.5.monte-carlo.scm")

(define (random-in-range low high)
  (let ((range (abs (- high low))))
       (+ low (random range))))

(define (estimate-integral x1 y1 x2 y2 p)
  (define (rand-pair) (cons (random-in-range x1 x2) (random-in-range y1 y2)))
  (define (test-stream pair)
    (cons-stream pair
                 (test-stream (rand-pair))))
  (define experiment-stream
    (stream-map (lambda (pair) (p (car pair) (cdr pair)))
                (test-stream (rand-pair))))
  (stream-map (lambda (x) (* x (* (abs (- x2 x1)) (abs (- y2 y1)))))
              (monte-carlo experiment-stream 0 0)))

;; test code
(define (circle? x y) (< (+ (* x x) (* y y)) 25))
(define sample (estimate-integral -10.0 -10.0 10.0 10.0 circle?))
(display-line "ex3.82")
(display-line (* 3.1415926 25))
(display-line (stream-ref sample 1000))
