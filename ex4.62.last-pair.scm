(load "4.4.4.query-system.scm")
(load "4.4.1.database.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
; answer
(assert! (rule (last-pair (?x) (?x))))
(assert! (rule (last-pair (?car . ?cdr) ?ans)
               (last-pair ?cdr ?ans)))
; test
(last-pair (3) ?x)
(last-pair (1 2 3) ?x)
(last-pair (2 ?x) (3))
; (last-pair ?x (3)) ;; infinite loop
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
