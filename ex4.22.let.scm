(load "4.1.7.analyze.scm")

(define (let? exp) (tagged-list? exp 'let))

(define (analyze-let exp)
  (let ((vars (map car (cadr exp)))
        (vals (map cadr (cadr exp)))
        (body (cddr exp)))
       (analyze-application (cons (make-lambda vars body) vals))))

(define (analyze exp)
  (cond ((self-evaluating? exp) (analyze-self-evaluating exp))
        ((quoted? exp) (analyze-quoted exp))
        ((variable? exp) (analyze-variable exp))
        ((assignment? exp) (analyze-assignment exp))
        ((definition? exp) (analyze-definition exp))
        ((if? exp) (analyze-if exp))
        ((lambda? exp) (analyze-lambda exp))
        ((begin? exp) (analyze-sequence (begin-actions exp)))
        ((cond? exp) (analyze (cond->if exp)))
        ((let? exp) (analyze-let exp))
        ((application? exp) (analyze-application exp))
        (else (error "[M-ana] Unknow expression type -- ANALYZE" exp))))

;; test code
;; (let ((a (cons 3 2)) (b 5)) (cons a b))
