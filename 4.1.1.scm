(load "4.1.3.scm")
(load "4.1.2.scm")
(load "4.1.4.scm")
(load "4.1.1.inner.scm")
;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 4.1.1
(define (g-eval exp env)
  (cond ((self-evaluating? exp) exp)
        ((variable? exp) (lookup-variable-value exp env))
        ((quoted? exp) (text-of-quotation exp))
        ((assignment? exp) (eval-assignment exp env))
        ((definition? exp) (eval-definition exp env))
        ((if? exp) (eval-if exp env))
        ((lambda? exp) (make-procedure (lambda-parameters exp)
                                       (lambda-body exp)
                                       env))
        ((begin? exp) (eval-sequence (begin-actions exp) env))
        ((cond? exp) (g-eval (cond->if exp) env))
        ((application? exp) (g-apply (g-eval (operator exp) env)
                                     (list-of-values (operands exp) env)))
        (else (error "[M-env]Unknown expression type -- EVAL" exp))))


(define (g-apply procedure arguments)
  (cond ((primitive-procedure? procedure) (apply-primitive-procedure procedure arguments))
        ((compound-procedure? procedure) (eval-sequence (procedure-body procedure)
                                                        (extend-environment
                                                               (procedure-parameters procedure)
                                                               arguments
                                                               (procedure-environment procedure))))
        (else (error "[M-env]Unknow procedure type -- APPLY" procedure))))
