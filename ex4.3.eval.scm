(load "4.1.1.scm")

;; table operations
(load "3.3.3.scm")

(define can-process-table (make-table))
(define do-process-table (make-table))
; quote
(insert! 'quote quoted? can-process-table)
(insert! 'quote (lambda (exp env)
                  (text-of-quotation exp))
                do-process-table)
; assignment
(insert! 'set! assignment? can-process-table)
(insert! 'set! eval-assignment do-process-table)
; definition
(insert! 'define definition? can-process-table)
(insert! 'define eval-definition do-process-table)
; if
(insert! 'if if? can-process-table)
(insert! 'if eval-if do-process-table)
; lambda
(insert! 'lambda lambda? can-process-table)
(insert! 'lambda (lambda (exp env)
                   (make-procedure (lambda-parameters exp)
                                   (lambda-body exp)
                                   env))
                 do-process-table)
; begin
(insert! 'begin begin? can-process-table)
(insert! 'begin (lambda (exp env)
                  (eval-sequence (begin-actions exp) env))
                do-process-table)
; cond
(insert! 'cond cond? can-process-table)
(insert! 'cond (lambda (exp env)
                 (g-eval (cond->if exp) env))
               do-process-table)

;; answers
(define (can-process? exp)
  (let ((f (lookup (car exp) can-process-table)))
       (if f
           (f exp)
           false)))

(define (do-process exp env)
  (let ((f (lookup (car exp) do-process-table)))
       (if f
           (f exp env)
           (error "do-process: not found -- exp:" exp))))

(define (g-eval exp env)
  (cond ((self-evaluating? exp) exp)
        ((variable? exp) (lookup-variable-value exp env))
        ((can-process? exp) (do-process exp env))
        ((application? exp) (g-apply (g-eval (operator exp) env)
                                     (list-of-values (operands exp) env)))
        (else (error "Unkown expression type -- ex4.3 EVAL" exp))))

;; tester
(driver-loop)
