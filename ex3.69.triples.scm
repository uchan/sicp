(load "ex3.67.pairs.scm")

(define (all-triples s t u)
  (cons-stream (list (stream-car s) (stream-car t) (stream-car u))
               (interleave (stream-map (lambda (x) (cons (stream-car s) x))
                                       (stream-cdr (pairs t u)))
                           (all-triples (stream-cdr s) (stream-cdr t) (stream-cdr u)))))

(define (triples s t u)
  (stream-filter (lambda (x) (and (<= (car x) (cadr x)) (<= (cadr x) (caddr x))))
                 (all-triples s t u)))

(define pythagoras-triples (stream-filter (lambda (x) (= (square (caddr x))
                                                        (+ (square (car x)) (square (cadr x)))))
                                         (triples integers integers integers)))


;; test code
(display-line 'triples-test)
(display-line (stream-head (all-triples integers integers integers) 30))
(display-line (stream-head (triples integers integers integers) 30))
(stream-head pythagoras-triples 4)
