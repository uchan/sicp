;; thinking
;;
;; 1
;; 1 1
;; 1 2 1
;; 1 3 3 1
;; 1 4 6 4 1
;; 
;; pascal(x,0)=1
;; pascal(x,x)=1
;; pascal(2,1)=pascal(1,0)+pascal(1,1)=1+1
;; pascal(4,1)=pascal(3,0)+pascal(3,1)=1+pascal(3,1)
;;             pascal(3,1)=pascal(2,0)+pascal(2,1)=1+pascal(2,1)
;; pascal(4,2)=pascal(3,1)+pascal(3,2)
;;             pascal(3,2)=pascal(2,1)+pascal(2,2)=pascal(2,1)+1
;; pascal(x,y)=pascal(x-1,y-1)+pascal(x-1,y)
;; 

;; conclusion
;; 
;; pascal(row, col)
;; row:0-n
;; col:0-row
;;
;; pascal(n, 0) = 1
;; pascal(n, n) = 1
;; pascal(r, c) = pascal(r-1, c-1) + pascal(r-1,c)


(define (pascal row col)
    (cond ((= col 0) 1)
          ((= row col) 1)
          (else (+ (pascal (- row 1) (- col 1))
                   (pascal (- row 1) col)))))
