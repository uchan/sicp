(load "ex3.60.mul-series.scm")

;; (1/f)'=(-f'/f^2)=(-f')*(1/f^2)
(define (inverse s)
  (define (derivative s) (mul-streams integers (stream-cdr s)))
  (cons-stream 1 (integrate-series (mul-series (scale-stream (derivative s) -1)
                                               (inverse (mul-series s s))))))

;; tester 1
(newline)(newline)(newline)(newline)
(define inverse-ones (cons-stream 1 (cons-stream -1 inverse-ones)))
(display-line 'exp-inverse-by-def)
(display-line (stream-head (mul-streams exp-series inverse-ones) 10))
(display-line 'exp-inverse-by-proc)
;;(stream-head (stream-map display-line (inverse exp-series)) 10) ;; for debug
(display-line (stream-head (inverse exp-series) 10))


;; tester 2
(newline)(newline)
(display-line 'tan-x)
(display-line '(0 1 0 1/3 0 2/15 0 17/315 0  62/2835))
(display-line (stream-head (mul-series sine-series (inverse cosine-series)) 10))
