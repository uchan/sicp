(define (square x) (* x x))

;; square check
(define (square-check a n)
  (if (and (not (= a 1))
           (not (= a (- n 1)))
           (= 1 (remainder (square a) n)))
      0
      (remainder (square a) n)))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (square-check (expmod base (/ exp 2) m) m))
        (else
         (remainder (* base (expmod base (- exp 1) m)) m))))

(define (MR-test a n)
  (= (expmod a (- n 1) n) 1))

(define (MR-prime? n times)
  (cond ((= 0 times) #t)
        ((MR-test (+ 1 (random (- n 1))) n)
         (MR-prime? n (- times 1)))
        (else #f)))
