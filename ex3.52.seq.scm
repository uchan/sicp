(load "3.5.1.stream.scm")

(define sum 0)
(define (accum x) (set! sum (+ x sum)) sum)

(define seq (stream-map accum (stream-enumerate-interval 1 20)))
(define y (stream-filter even? seq))
(define z (stream-filter (lambda (x) (= (remainder x 5) 0)) seq))

(display-line 'before-ref-7)
(display-line (stream-ref y 7))
(display-line 'before-disp-z)
(display-stream z)
