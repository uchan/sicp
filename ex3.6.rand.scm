(define (rand action)
  (cond ((eq? action 'gen)
         (lambda () (let ((x random-init)) (lambda () (set! x (rand-update x)) x))))
        ((eq? action 'reset)
         (lambda (new) (let ((x new)) (lambda () (set! x (rand-update x)) x))))
        (else (error "what?:" action))))

(define random-init 1)
(define (random-update x) (reminder 3 (+ 4 (* 5 x))))
