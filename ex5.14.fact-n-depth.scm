(load "5.2.4.monitor.scm")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; test
(load "5.1.4.factorial.scm")
;  setting input
(define (show-fact n)
  (newline)
  ((fact-machine 'stack) 'initialize)
  (display (list 'n '= n))
  (set-register-contents! fact-machine 'n n)
  (start fact-machine)
  (display "\nmachine result:")
  (display (get-register-contents fact-machine 'val))
  (stack-show fact-machine))
(map show-fact '(1 2 3 4 5 6 7 8 9 10))
