(load "ex2.12.percent.scm")

(define (tester a ap b bp)
  (let ((A (make-center-percent a ap))
        (B (make-center-percent b bp)))
       (display (div-interval A A))
       (display ",")
       (display (div-interval A B))))
