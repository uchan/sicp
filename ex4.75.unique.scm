(load "4.4.4.query-system.scm")

;; replace original functions
(define (unique-content exps) (car exps))
(define (uniquely-asserted query frame-stream)
  (let ((result (qeval (unique-content query) frame-stream)))
       (if (stream-null? (stream-cdr result))
           result
           frame-stream)))
(put 'unique 'qeval uniquely-asserted)

;; begin process data
(load "4.4.1.database.scm")

;; begin test code
(define test-flag true)
(define sys-read read)
(define test-code '(
(and (job ?x ?j) (unique (job ?anyone ?j)))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
