(load "4.1.1.scm")

(define (left-list-of-values exp env)
  (display 'in-left)(newline)
  (if (no-operands? exp)
      '()
      (let ((left (g-eval (first-operand exp) env)))
           (cons left (left-list-of-values (rest-operands exp) env)))))

(define (right-list-of-values exp env)
  (display 'in-right)(newline)
  (if (no-operands? exp)
      '()
      (let ((right (right-list-of-values (rest-operands exp) env)))
           (cons (g-eval (first-operand exp) env) right))))

;; tester: overwrite current value
(define (text-of-quotation exp) 
  (display "debug:")(display exp) (newline) (cdr exp))
(define list-of-values left-list-of-values)
;;(define list-of-values right-list-of-values)

;; begin loop
(driver-loop)
