(load "3.5.2.infinite.scm")

(load "ex3.54.mul.scm")
(load "ex3.59.series.scm")

(define (mul-series s1 s2)
  (define (derivative s) (mul-streams integers (stream-cdr s)))
  (cons-stream (* (stream-car s1) (stream-car s2))
               (integrate-series (add-streams (mul-series (derivative s1) s2)
                                              (mul-series s1 (derivative s2))))))


;; tester
(newline)(newline)(newline)(newline)
(display-line 'test-exp2)
(define exp2 (cons-stream 1 (scale-stream exp2 2)))
(display-line (stream-head exp2 20))
(display-line 'double-exp-series-by-def)
(display-line (stream-head (mul-streams exp-series exp2) 10))
(display-line 'double-exp-series-by-mul)
(display-line (stream-head (mul-series exp-series exp-series) 10))

;; tester 2
(newline)(newline)
(display-line 'test-cos2) ;; (cosx)^2=(1+cos2x)/2=1/2+cos2x/2
(define (add-const c s) (cons-stream (+ c (stream-car s)) (stream-cdr s)))
(display-line (stream-head (add-const (/ 1 2) (scale-stream (mul-streams cosine-series exp2) (/ 1 2))) 10))
(display-line (stream-head (mul-series cosine-series cosine-series) 10))


;; tester 3
(newline)(newline)
(display-line 'test-cosx*sinx) ;; cosx*sinx=(sin2x)/2
(display-line (stream-head (scale-stream (mul-streams sine-series exp2) (/ 1 2)) 10))
(display-line (stream-head (mul-series sine-series cosine-series) 10))
