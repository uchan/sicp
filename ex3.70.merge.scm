(load "ex3.67.pairs.scm")

(define (merge-weighted s1 s2 weight)
  (cond ((stream-null? s1) s2)
        ((stream-null? s2) s1)
        (else (let ((s1car (stream-car s1)) (s2car (stream-car s2)))
                   (if (< (weight s1car) (weight s2car))
                       (cons-stream s1car (merge-weighted (stream-cdr s1) s2 weight))
                       (cons-stream s2car (merge-weighted s1 (stream-cdr s2) weight)))))))

(define (weighted-pairs s t weight)
  (cons-stream (list (stream-car s) (stream-car t))
               (merge-weighted (stream-map (lambda (x) (list (stream-car s) x)) (stream-cdr t))
                               (weighted-pairs (stream-cdr s) (stream-cdr t) weight)
                               weight)))

;; weight condition a
(define (weight-a pair) (+ (car pair) (cadr pair)))
(define test-a (weighted-pairs integers integers weight-a))

;; weight condition b
(define (weight-b pair)
  (let ((i (car pair)) (j (cadr pair)))
           (+ (* 2 i) (* 3 j) (* 5 i j))))
(define (test-b)
  (define streamb (stream-filter (lambda (x) (or (= 0 (remainder x 2))
                                                 (= 0 (remainder x 3))
                                                 (= 0 (remainder x 5))))
                                 integers))
  (weighted-pairs streamb streamb weight-b))

;; test code
(display-line 'ex3-70)
(display-line (stream-head test-a 20))
(display-line (stream-head (test-b) 20))
