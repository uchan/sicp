(load "4.2.2.thunk.scm")

(define primitive-procedures
  (list (list 'null? null?) (list 'equal? equal?)
        (list '+ +) (list '- -) (list '* *) (list '/ /) (list '> >) (list '< <) (list '= =)
        (list 'list list) (list 'newline newline) (list 'display display)))
