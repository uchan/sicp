(load "4.3.3.scm")

;;========================================================
;; answer code
;;========================================================
;; for 'permanent-set!' symbol
(define (permanent-set? exp) (tagged-list? exp 'permanent-set!))
(define (permanent-set-variable exp) (cadr exp))
(define (permanent-set-value exp) (caddr exp))

;; for 'if-fail' symbol
(define (if-fail? exp) (tagged-list? exp 'if-fail))
(define (if-fail-consequent exp) (cadr exp))
(define (if-fail-alternative exp) (caddr exp))

;-------------------------------------
; main answer
(define (analyze-permanent-set exp)
  (let ((var (permanent-set-variable exp))
        (vproc (analyze (permanent-set-value exp))))
       (lambda (env succeed fail)
         (vproc env
                (lambda (val fail2) ; success for <permanent-set>: set value
                  (set-variable-value! var val env)
                  (succeed 'ok fail2))
                fail))))

(define (analyze-if-fail exp)
  (let ((cproc (analyze (if-fail-consequent exp)))
        (aproc (analyze (if-fail-alternative exp))))
       (lambda (env succeed fail)
         (cproc env
                (lambda (val fail2) ; success for <if-fail>: return value
                  (succeed val fail2))
                (lambda ()          ; failure for <if-fail>: do alternative
                  (aproc env succeed fail))))))

;-------------------------------------
;; main function
(define (analyze exp)
  (cond ((self-evaluating? exp) (analyze-self-evaluating exp))
        ((quoted? exp) (analyze-quoted exp))
        ((variable? exp) (analyze-variable exp))
        ((assignment? exp) (analyze-assignment exp))
        ((permanent-set? exp) (analyze-permanent-set exp))
        ((definition? exp) (analyze-definition exp))
        ((if? exp) (analyze-if exp))
        ((if-fail? exp) (analyze-if-fail exp))
        ((not? exp) (analyze (not->if (get-aon-action exp))))
        ((or? exp) (analyze (or->if (get-aon-action exp))))
        ((and? exp) (analyze (and->if (get-aon-action exp))))
        ((lambda? exp) (analyze-lambda exp))
        ((begin? exp) (analyze-sequence (begin-actions exp)))
        ((cond? exp) (analyze (cond->if exp)))
        ((let? exp) (analyze-let exp))
        ((amb? exp) (analyze-amb exp))
        ((application? exp) (analyze-application exp))
        (else (error "[M-amb] Unknow expression type -- ANALYZE" exp))))

;;========================================================
;; test code
;;========================================================
(define test-flag true)
(define sys-read read)
(define test-code '(
;; pre-defined
(define (require p) (if (not p) (amb)))
(define (an-element-of items)
  (require (not (null? items)))
  (amb (car items) (an-element-of (cdr items))))
;; text code
(define (square x) (* x x))
(define (divides? a b) (= (remainder b a) 0))
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))
(define (smallest-divisor n) (find-divisor n 2))
(define (prime? n) (= n (smallest-divisor n)))
(define (prime-sum-pair list1 list2)
  (let ((a (an-element-of list1))
        (b (an-element-of list2)))
       (require (prime? (+ a b)))
       (list a b)))
;; begin test
(let ((pairs (quote)))
     (if-fail (let ((p (prime-sum-pair (quote 1 3 5 8) (quote 20 35 110))))
                   (permanent-set! pairs (cons p pairs))
                   (amb))
              pairs))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (begin (let ((input (car test-code)))
                  (set! test-code (cdr test-code))
                  (display input)(newline)
                  input))
      (sys-read)))

(driver-loop)
