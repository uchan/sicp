(load "4.4.4.query-system.scm")
(load "4.4.1.database.scm")

(define test-flag true)
(define sys-read read)
(define test-code '(
;; import 4.4.1.append-to-from
;; rule
(assert! (rule (append-to-from () ?y ?y)))
(assert! (rule (append-to-from (?u . ?v) ?y (?u . ?z))
               (append-to-from ?v ?y ?z)))
;; answer
(assert! (rule (reverse () ())))
(assert! (rule (reverse (?first . ?rest) ?ans)
               (and (reverse ?rest ?r-rest)
                    (append-to-from ?r-rest (?first) ?ans))))
;; test
(reverse (1 2 3) ?x)
(reverse ?x (1 2 3))
))

(define (read)
  (if (and test-flag (not (null? test-code)))
      (if (not (null? data-base))   ;; input data-base first
          (let ((input (car data-base)))
               (set! data-base (cdr data-base))
               (display input)(newline)
               input)
          (let ((input (car test-code)))
               (set! test-code (cdr test-code))
               (display input)(newline)
               input))
      (sys-read)))

(query-driver-loop)
