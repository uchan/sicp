(load "1.2.6.prime.scm")
(load "1.2.6.fast-prime.scm")

(define (carmichael n)
  (if (prime? n)   ;; prime is not carmichael
      #f
      (carmichael-test 1 n)))

(define (carmichael-test a n)
  (cond ((= a n) #t)
        ((= (expmod a n n) a)
         (carmichael-test (+ 1 a) n))
        (else #f)))
