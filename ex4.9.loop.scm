(load "4.1.1.scm")

;; (while <predicate> <body>)
;; ==> <body> --> (lambda () <body>)
;;     <predicate> --> (if <predicate> (<body> + (if <predicate> ...)) true)
;;
;; ==> (define (while-body)
;;       (if <predicate>
;;           (begin (lambda () <body>) (while-body))
;;           true))
;; ==> remove the name 'while-body' by 'Y combinator'
