(define (deep-reverse lst)
  (show lst)
  (cond ((null? lst) lst)
        ((not (pair? lst)) lst)
        (else (gappend (deep-reverse (cdr lst)) (list (deep-reverse (car lst)))))))


;; for debug
(define (gappend lst1 lst2)
  (display "append:")(display lst1)(display ",")(display lst2)(newline)
  (append lst1 lst2))

(define (show lst)
  (display "lst:")(display lst)(newline))
