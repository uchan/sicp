(load "5.2.1.machine-model.scm")

;; answer
;; stack
(define (make-stack)
  (let ((s '()))
       (define (push val name)
         (let ((reg-stack (assoc name s)))
              (if reg-stack
                  (set-cdr! reg-stack (cons val (cdr reg-stack)))
                  (set! s (cons (cons name (list val)) s)))))
       (define (pop name)
         (if (null? s)
             (error "Empty-stack -- POP")
             (let ((reg-stack (assoc name s)))
                  (if reg-stack
                      (let ((top (car (cdr reg-stack))))
                           (set-cdr! reg-stack (cdr (cdr reg-stack)))
                           top)
                      (error "Name not exist -- POP:" name)))))
       (define (initialize) (set! s '()) 'done)
       (define (dispatch message)
         (cond ((eq? message 'push) push)
               ((eq? message 'pop) pop)
               ((eq? message 'initialize) (initialize))
               (else (error "Unknown request -- STACK" message))))
       dispatch))
(define (pop stack name) ((stack 'pop) name))
(define (push stack value name) ((stack 'push) value name))

;; new make-save/restore
(define (make-save inst machine stack pc)
  (let ((reg (get-register machine (stack-inst-reg-name inst))))
       (lambda ()
            (push stack (get-contents reg) (stack-inst-reg-name inst))
            (advance-pc pc))))
(define (make-restore inst machine stack pc)
  (let ((reg (get-register machine (stack-inst-reg-name inst))))
       (lambda ()
            (set-contents! reg (pop stack (stack-inst-reg-name inst)))
            (advance-pc pc))))

;; test
(define my-machine
  (make-machine
    '(x y)
    (list (list '- -) (list '= =) (list '* *))
    '(start
        (assign x (const 1))
        (assign y (const 2))
        (save y)
        (save x)
        (restore y)
      end)
  )
)
;  setting input
(start my-machine)
(display "machine result:")
(display (get-register-contents my-machine 'y))
(newline)
