(load "ex4.22.let.scm")

(define (amb? exp) (tagged-list? exp 'amb))
(define (amb-choices exp) (cdr exp))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; re-write analyze-x
;- - - - - - - - - - - - - - - - -
;  level 0: simple
(define (analyze-self-evaluating exp)
  (lambda (env succeed fail) (succeed exp fail)))

(define (analyze-quoted exp)
  (let ((qval (text-of-quotation exp)))
       (lambda (env succeed fail) (succeed qval fail))))

(define (analyze-variable exp)
  (lambda (env succeed fail) (succeed (lookup-variable-value exp env) fail)))

(define (analyze-lambda exp)
  (let ((vars (lambda-parameters exp))
        (bproc (analyze-sequence (lambda-body exp))))
       (lambda (env succeed fail) (succeed (make-procedure vars bproc env) fail))))
;- - - - - - - - - - - - - - - - -
;  level 1: if & sequence
(define (analyze-if exp)
  (let ((pproc (analyze (if-predicate exp)))
        (cproc (analyze (if-consequent exp)))
        (aproc (analyze (if-alternative exp))))
       (lambda (env succeed fail)
         (pproc env
                ;; success continuation for evaluating the predicate to obtain <pred-value>
                (lambda (pred-value fail2)
                  (if (true? pred-value)
                      (cproc env succeed fail2)
                      (aproc env succeed fail2)))
                ;; failure continuation for evaluating the predicate
                fail))))

(define (analyze-sequence exps)
  (define (sequentially a b)
    (lambda (env succeed fail)
      (a env
         ;; success continuation for calling a
         (lambda (a-value fail2)
           (b env succeed fail2))
         ;; failure continuation for calling b
         fail)))
  (define (loop first-proc rest-procs)
    (if (null? rest-procs)
        first-proc
        (loop (sequentially first-proc (car rest-procs)) (cdr rest-procs))))
  (let ((procs (map analyze exps)))
       (if (null? procs)
           (error "[M-amb] Empty sequence -- analyze-sequence" exps))
       (loop (car procs) (cdr procs))))
;- - - - - - - - - - - - - - - - -
;  level 2: define & assignment
(define (analyze-definition exp)
  (let ((var (definition-variable exp))
        (vproc (analyze (definition-value exp))))
       (lambda (env succeed fail)
         (vproc env
                (lambda (val fail2)
                  (define-variable! var val env)
                  (succeed 'ok fail2))
                fail))))

(define (analyze-assignment exp)
  (let ((var (assignment-variable exp))
        (vproc (analyze (assignment-value exp))))
       (lambda (env succeed fail)
         (vproc env
                (lambda (val fail2)	; success continuation for vproc
                  (let ((old-value (lookup-variable-value var env)))
                       (set-variable-value! var val env)
                       (succeed 'ok
                                (lambda ()  ; failure continuation for rollback to old-value
                                  (set-variable-value! var old-value env)
                                  (fail2)))))
                fail))))
                                
;- - - - - - - - - - - - - - - - -
;  level 3: application
(define (execute-application proc args succeed fail)
  (cond ((primitive-procedure? proc) (succeed (apply-primitive-procedure proc args) fail))
        ((compound-procedure? proc)
         ((procedure-body proc)
          (extend-environment (procedure-parameters proc) args (procedure-environment proc))
          succeed
          fail))
        (else (error "[M-amb] Unknown procedure type -- EXECUTE-APPLICATION" proc))))

(define (get-args aprocs env succeed fail)
  (if (null? aprocs)
      (succeed '() fail)
      ((car aprocs) env
                    ;; success continuation for this <aproc>
                    (lambda (arg fail2)
                      (get-args (cdr aprocs) env
                                ;; success continuation for recursive call to get args
                                (lambda (args fail3)
                                  (succeed (cons arg args) fail3))
                                fail2))
                    fail)))

(define (analyze-application exp)
  (let ((fproc (analyze (operator exp)))
        (aprocs (map analyze (operands exp))))
       (lambda (env succeed fail)
         (fproc env
                (lambda (proc fail2) ;; success continuation to run the result of <fproc>
                  (get-args aprocs env
                            (lambda (args fail3) ;; success continuation to run apply
                              (execute-application proc args succeed fail3))
                            fail2))
                fail))))
;- - - - - - - - - - - - - - - - -
;  level 4: amb
(define (analyze-amb exp)
  (let ((cprocs (map analyze (amb-choices exp))))
       (lambda (env succeed fail)
         (define (try-next choices)
           (if (null? choices)
               (fail)
               ((car choices) env succeed (lambda () (try-next (cdr choices))))))
         (try-next cprocs))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; extra
(define (not? exp) (tagged-list? exp 'not))
(define (and? exp) (tagged-list? exp 'and))
(define (or? exp) (tagged-list? exp 'or))
(define (get-aon-action exp) (cdr exp))
(define (not->if exp)
  (if (null? exp)
      (error "[M] not without argument")
      (if (null? (cdr exp))
          (make-if (car exp) 'false 'true)
          (make-if exp 'false 'true))))
(define (or->if exp)
  (if (null? exp)
      'false
      (make-if (car exp) 'true (or->if (cdr exp)))))
(define (and->if exp)
  (if (null? exp)
      'true
      (make-if (car exp) (and->if (cdr exp)) 'false)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main
(define (analyze exp)
  (cond ((self-evaluating? exp) (analyze-self-evaluating exp))
        ((quoted? exp) (analyze-quoted exp))
        ((variable? exp) (analyze-variable exp))
        ((assignment? exp) (analyze-assignment exp))
        ((definition? exp) (analyze-definition exp))
        ((if? exp) (analyze-if exp))
        ((not? exp) (analyze (not->if (get-aon-action exp))))
        ((or? exp) (analyze (or->if (get-aon-action exp))))
        ((and? exp) (analyze (and->if (get-aon-action exp))))
        ((lambda? exp) (analyze-lambda exp))
        ((begin? exp) (analyze-sequence (begin-actions exp)))
        ((cond? exp) (analyze (cond->if exp)))
        ((let? exp) (analyze-let exp))
        ((amb? exp) (analyze-amb exp))
        ((application? exp) (analyze-application exp))
        (else (error "[M-amb] Unknow expression type -- ANALYZE" exp))))

(define (ambeval exp env succeed fail)
  ((analyze exp) env succeed fail))

(define input-prompt ";;; Amb-Eval input:")
(define output-prompt ";;; Amb-Eval value:")
(define (driver-loop)
  (define (internal-loop try-again)
    (prompt-for-input input-prompt)
    (let ((input (read)))
         (if (eq? input 'try-again)
             (try-again)
             (begin (newline) (display ";;; Staring a new problem ")
                    (ambeval input the-global-environment
                             ;; ambeval success
                             (lambda (val next-alternative)
                               (announce-output output-prompt)
                               (user-print val)
                               (internal-loop next-alternative))
                             ;; ambeval failure
                             (lambda ()
                               (announce-output ";;; There are no more values of ")
                               (user-print input)
                               (driver-loop)))))))
  (internal-loop (lambda () (newline) (display ";;; There is no current problem") (driver-loop))))
