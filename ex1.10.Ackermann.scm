(define (A x y)
  (cond ((= y 0) 0)
	((= x 0) (* 2 y))
	((= y 1) 2)
	(else (A (- x 1) (A x (- y 1))))))
(define (f n) (A 0 n))
(define (g n) (A 1 n))
(define (h n) (A 2 n))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; f(n)=(A 0 n)=(* 2 n)=2n
;; g(n)=(A 1 n)=(A 0 (A 1 (n-1)))=f(g(n-1))=2*g(n-1)=2^n
;; h(n)=(A 2 n)=(A 1 (A 2 (n-1)))=g(h(n-1))=2^(h(n-1))
