(load "1.3.3.fixed-point.scm")
(load "ex1.43.repeated.scm")

(define (average-damp f) (lambda (x) (average x (f x))))

;; tester
(define (tester m n c)
  (define (show guess) (display "guess:")(display guess)(display "\n"))
  (define x 2)
  (define (func y) (/ x (expt y (- m 1))))
  (define (n-avg) (repeated average-damp n))
  (define (f guess) (((n-avg) func) guess))
  (define (try guess count)
    (show guess)
    (if (= 0 count) guess
        (try (f guess) (- count 1))))
  (try 1.0 c))

;;;;;;;;;;;;;;;;;;;;;;;;;
;; sqrtn begin
;  n times average-damp
(define (n-avg n f) ((repeated average-damp n) f))
;  relationship of (root m) and (n times average-damp)
;  n>=(lg 2 m)
(define (count-avg m)
  (define (lg x) (/ (log x) (log 2)))
  (ceiling (lg m)))
;  main function
(define (sqrtn n x)
  (define (n-func y) (/ x (expt y (- n 1))))
  (fixed-point (n-avg (count-avg n) n-func) 1.0))
