(define (iterative-improve enough? improve)
  (lambda (guess)
    (define (iter result)
      (if (enough? result) result
          (iter (improve result))))
    (iter guess)))

;; sqrt
(define (sqrt x)
  (define (improve guess) (/ (+ guess (/ x guess)) 2))
  (define (enough? guess)
    (< (abs (- guess (improve guess))) 0.001))
  ((iterative-improve enough? improve) 1.0))

;; fixed-point
(define (fixed-point f)
  (define (improve guess) (/ (+ guess (f guess)) 2))
  (define (enough? guess)
    (< (abs (- guess (improve guess))) 0.001))
  ((iterative-improve enough? improve) 1.0))
