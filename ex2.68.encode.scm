(load "ex2.67.decode.scm")

;; textbook
(define (encode message tree)
  (if (null? message) '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

;; answer
(define (encode-symbol sym tree)
  (define (contains ele set)
    (pair? (member ele set)))
  (define (getbits symbol branch result)
    (cond ((and (leaf? branch) (contains symbol (symbols branch))) result)
          ((contains symbol (symbols (left-branch branch)))
           (getbits symbol (left-branch branch) (append result '(0))))
          ((contains symbol (symbols (right-branch branch)))
           (getbits symbol (right-branch branch) (append result '(1))))
          (else (error "what? result:" result))))
  (getbits sym tree '()))
