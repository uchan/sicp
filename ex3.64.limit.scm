(load "3.5.3.paradigm.scm")

(define (stream-limit s tolerance)
  (let ((i0 (stream-ref s 0))
        (i1 (stream-ref s 1)))
       (if (< (abs (- i1 i0)) tolerance)
           i1
           (stream-limit (stream-cdr s) tolerance))))

;; test code
(define (sqrt-ex x tolerance)
  (stream-limit (sqrt-stream x) tolerance))
